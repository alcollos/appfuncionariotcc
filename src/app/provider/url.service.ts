import { Injectable } from "@angular/core";
import { AlertController, LoadingController } from "@ionic/angular";
import { analyzeAndValidateNgModules } from "@angular/compiler";

@Injectable({
  providedIn: "root"
})
export class UrlService {
  //url: string = 'http://teratags.com/tcc/php/';

  url_login: string = "http://teratags.com/tcc/php/";
  url_carrinho: string = "http://teratags.com/tcc/php/";
  url_estoque: string = "http://teratags.com/tcc/php/";
  url_cadastro: string = "http://teratags.com/tcc/php/";
  url_select: string = "http://teratags.com/tcc/php/";
  url_favorito: string = "http://teratags.com/tcc/php/";
  url: string = "http://teratags.com/tcc/php/img_perfil/";
  urlFoto: string = "http://teratags.com/tcc/php/img_produtos/";

  loading = false;

  constructor(
    public alert: AlertController,
    public loadingCrtl: LoadingController
  ) {}
  getUrlSelect() {
    return this.url_select;
  }

  getUrlLogin() {
    return this.url_login;
  }
  getUrlCarrinho() {
    return this.url_carrinho;
  }
  getUrlEstoque() {
    return this.url_estoque;
  }
  getUrlCadastro() {
    return this.url_cadastro;
  }

  async alertas(titulo, msg) {
    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: ["OK"]
    });
    await alert.present();
  }

  async exibirLoading() {
    this.loading = true;
    return await this.loadingCrtl
      .create({
        message: "Buscando dados..."
      })
      .then(a => {
        a.present().then(() => {
          if (!this.loading) {
            a.dismiss().then(() => {});
          }
        });
      });
  }

  async fecharLoding() {
    this.loading = false;
    return await this.loadingCrtl.dismiss().then(() => {});
  }
}
