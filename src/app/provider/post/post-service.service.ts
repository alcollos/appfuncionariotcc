import { Injectable } from "@angular/core";

import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../../provider/url.service";
import {
  NavController,
  Platform,
  LoadingController,
  ToastController,
  ActionSheetController
} from "@ionic/angular";
import { ServiceUserService } from "../../provider/service-user.service";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ActivatedRoute } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class PostServiceService {
  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService,
    public serviceUser: ServiceUserService,
    public nfce: NFC,

    // tslint:disable-next-line: max-line-length
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public ndef: Ndef
  ) {}

  postPublish(valor) {
    console.log("inserindo");
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "insertProduto.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postEdit(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "editProduto.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postEditQuantidadeCarrinho(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() + "atualizarQuantidadeCarrinho.php",
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postVerificarQuantidade(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "verificarCompra.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  adicionarQuantidadeVenda(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() + "adicionarQuantidadesVendas.php",
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
  postEditQuantidade(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() + "atualizarQuantidadeP.php",
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postSelecionados(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "insertSelecionados.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postCaracteristicas(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() + "insetCaracteristicas.php",
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postVendaFeita(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "postVendaFeita.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
  deleteCarAll(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlCarrinho() + "deleteCARRINHOALL.php",
        codigo,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  deleteNotaFiscal(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlCarrinho() + "deleteNotaFiscal.php", codigo, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  deleteCompras(codigo) {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlCarrinho() + "deleteCompras.php", codigo, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }
}
