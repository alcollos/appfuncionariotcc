import { TestBed } from '@angular/core/testing';

import { GraficoMensalService } from './grafico-mensal.service';

describe('GraficoMensalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraficoMensalService = TestBed.get(GraficoMensalService);
    expect(service).toBeTruthy();
  });
});
