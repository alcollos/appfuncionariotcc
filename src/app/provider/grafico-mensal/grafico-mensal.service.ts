import { Injectable } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";

@Injectable({
  providedIn: "root"
})
export class GraficoMensalService {
  qtdJaneiro: any;
  qtdFevereiro: any;
  qtdMarço: any;
  qtdAbril: any;
  qtdMaio: any;
  qtdJunho: any;
  qtdJulho: any;
  qtdAgosto: any;
  qtdSetembro: any;
  qtdOutubro: any;
  qtdNovembro: any;
  qtdDezembro: any;

  valorJaneiro: any;
  valorFevereiro: any;
  valorMarco: any;
  valorAbril: any;
  valorMaio: any;
  valorJunho: any;
  valorJulho: any;
  valorAgosto: any;
  valorSetembro: any;
  valorOutubro: any;
  valorNovembro: any;
  valorDezembro: any;

  mediaJaneiroNPS: any;
  aMediaJaneiroNPS: Array<{
    media: any;
  }>;

  aMediaFevereiroNPS: Array<{
    media: any;
  }>;
  aMediaMarcoNPS: Array<{
    media: any;
  }>;
  aMediaAbrilNPS: Array<{
    media: any;
  }>;
  aMediaMaioNPS: Array<{
    media: any;
  }>;
  aMediaJunhoNPS: Array<{
    media: any;
  }>;
  aMediaJulhoNPS: Array<{
    media: any;
  }>;
  aMediaAgostoNPS: Array<{
    media: any;
  }>;
  aMediaSetembroNPS: Array<{
    media: any;
  }>;
  aMediaOutubroNPS: Array<{
    media: any;
  }>;
  aMediaNovembroNPS: Array<{
    media: any;
  }>;
  aMediaDezembroNPS: Array<{
    media: any;
  }>;

  mediaFevereiroNPS: any;
  mediaMarcoNPS: any;
  mediaAbrilNPS: any;
  mediaMaioNPS: any;
  mediaJunhoNPS: any;
  mediaJulhoNPS: any;
  mediaAgostoNPS: any;
  mediaSetembroNPS: any;
  mediaOutubroNPS: any;
  mediaNovembroNPS: any;
  mediaDezembroNPS: any;

  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public graficoDiario: GraficoDiarioService
  ) {}

  scriptQuantidadeMensal1(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=1&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdJaneiro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal2(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=2&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdFevereiro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal3(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=3&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdMarço = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal4(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=4&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdAbril = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal5(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=5&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdMaio = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal6(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=6&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdJunho = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal7(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=7&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdJulho = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal8(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=8&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdAgosto = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal9(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=9&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdSetembro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal10(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=10&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdOutubro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal11(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=11&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdNovembro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptQuantidadeMensal12(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=12&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdDezembro = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  scriptValorMensal1(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=1&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorJaneiro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal2(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=2&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorFevereiro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }

  scriptValorMensal3(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=3&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorMarco = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal4(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=4&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorAbril = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal5(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=5&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorMaio = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal6(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=6&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorJunho = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal7(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=7&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorJulho = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal8(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=8&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorAgosto = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal9(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=9&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorSetembro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal10(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=10&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorOutubro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal11(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=11&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorNovembro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptValorMensal12(anoSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHMensal.php?mes_selecionado=12&ano_selecionado=" +
          anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valorDezembro = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }

  scriptMediaMensal1(anoSelecionado) {
    this.aMediaJaneiroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=1"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaJaneiroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaJaneiroNPS = this.aMediaJaneiroNPS[0].media;
      });
  }
  scriptMediaMensal2(anoSelecionado) {
    this.aMediaFevereiroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=2"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaFevereiroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaFevereiroNPS = this.aMediaFevereiroNPS[0].media;
      });
  }
  scriptMediaMensal3(anoSelecionado) {
    this.aMediaMarcoNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=3"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaMarcoNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaMarcoNPS = this.aMediaMarcoNPS[0].media;
      });
  }
  scriptMediaMensal4(anoSelecionado) {
    this.aMediaAbrilNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=4"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaAbrilNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaAbrilNPS = this.aMediaAbrilNPS[0].media;
      });
  }
  scriptMediaMensal5(anoSelecionado) {
    this.aMediaMaioNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=5"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaMaioNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaMaioNPS = this.aMediaMaioNPS[0].media;
      });
  }
  scriptMediaMensal6(anoSelecionado) {
    this.aMediaJunhoNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=6"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaJunhoNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaJunhoNPS = this.aMediaJunhoNPS[0].media;
      });
  }
  scriptMediaMensal7(anoSelecionado) {
    this.aMediaJulhoNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=7"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaJulhoNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaJulhoNPS = this.aMediaJulhoNPS[0].media;
      });
  }
  scriptMediaMensal8(anoSelecionado) {
    this.aMediaAgostoNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=8"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaAgostoNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaAgostoNPS = this.aMediaAgostoNPS[0].media;
      });
  }
  scriptMediaMensal9(anoSelecionado) {
    this.aMediaSetembroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=9"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaSetembroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaSetembroNPS = this.aMediaSetembroNPS[0].media;
      });
  }
  scriptMediaMensal10(anoSelecionado) {
    this.aMediaOutubroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=10"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaOutubroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaOutubroNPS = this.aMediaOutubroNPS[0].media;
      });
  }
  scriptMediaMensal11(anoSelecionado) {
    this.aMediaNovembroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=11"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaNovembroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaNovembroNPS = this.aMediaNovembroNPS[0].media;
      });
  }
  scriptMediaMensal12(anoSelecionado) {
    this.aMediaDezembroNPS = [];
    let oi;

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          anoSelecionado +
          "&mesSelecionado=12"
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.aMediaDezembroNPS.push({
            media: data[i]["media"]
          });
        }
        this.mediaDezembroNPS = this.aMediaDezembroNPS[0].media;
      });
  }
}
