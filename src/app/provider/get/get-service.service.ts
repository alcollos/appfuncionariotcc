import { Injectable } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { PostServiceService } from "src/app/provider/post/post-service.service";
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: "root"
})
export class GetServiceService {
  aQuantidades: Array<{
    qtdTotal: any;
    qtdMensal: any;
    qtdDiaria: any;
  }>;

  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public postService: PostServiceService,
    public nav: NavController
  ) {}
}
