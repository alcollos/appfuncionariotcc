import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceUserService {
  idFunc = '';
  funcNome = '';
  funcEmail = '';
  funcTelefone = '';
  funcCPF = '';
  funcRG = '';
  funcEndereco = '';
  funcSenha = '';
  funcStatus = '';
  funcFoto = '';
  funcNivel = '';
  constructor() {}

  // Esse dados estão sendo pegos na tela de login
  setFuncNome(valor) {
    this.funcNome = valor;
  }

  getFuncNome() {
    return this.funcNome;
  }

  // GET; SET; COD_FUNC
  setFuncId(valor) {
    this.idFunc = valor;
  }

  getFuncId() {
    return this.idFunc;
  }

  // GET; SET; STATUS
  setFuncStatus(valor) {
    this.funcStatus = valor;
  }

  getFuncStatus() {
    return this.funcStatus;
  }

  // GET; SET; FOTO
  setFuncFoto(valor) {
    this.funcFoto = valor;
  }

  getFuncFoto() {
    return this.funcFoto;
  }

  // GET; SET; EMAIL
  setFuncEmail(valor) {
    this.funcEmail = valor;
  }

  getFuncEmail() {
    return this.funcEmail;
  }

  // GET; SET;TELEFONE
  setFuncTelefone(valor) {
    this.funcTelefone = valor;
  }

  getFuncTelefone() {
    return this.funcTelefone;
  }

  // GET; SET; cpf
  setFuncCPF(valor) {
    this.funcCPF = valor;
  }

  getFuncCPF() {
    return this.funcCPF;
  }

  // GET; SET; RG
  setFuncRG(valor) {
    this.funcRG = valor;
  }

  getFuncRG() {
    return this.funcRG;
  }

  // GET; SET; ENDERECO
  setFuncEndereco(valor) {
    this.funcEndereco = valor;
  }

  getFuncEndereco() {
    return this.funcEndereco;
  }

  // GET; SET; ENDERECO
  setFuncSenha(valor) {
    this.funcSenha = valor;
  }

  getFuncSenha() {
    return this.funcSenha;
  }

  // GET; SET; ENDERECO
  setFuncNivel(valor) {
    this.funcNivel = valor;
  }

  getFuncNivel() {
    return this.funcNivel;
  }
}
