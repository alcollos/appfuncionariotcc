import { Injectable } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";

@Injectable({
  providedIn: "root"
})
export class GraficoAnualService {
  qtd2010: any;
  qtd2011: any;
  qtd2012: any;
  qtd2013: any;
  qtd2014: any;
  qtd2015: any;
  qtd2016: any;
  qtd2017: any;
  qtd2018: any;
  qtd2019: any;

  valor2010: any;
  valor2011: any;
  valor2012: any;
  valor2013: any;
  valor2014: any;
  valor2015: any;
  valor2016: any;
  valor2017: any;
  valor2018: any;
  valor2019: any;
  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public graficoDiario: GraficoDiarioService
  ) {}

  apresentarQuantidadeAnual1() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2011"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2011 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2010"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2010 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual2() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2012"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2012 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual3() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2013"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2013 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual4() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2014"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2014 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual5() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2015"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2015 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual6() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2016"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2016 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual7() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2017"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2017 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual8() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2018"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2018 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarQuantidadeAnual9() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=2019"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtd2019 = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarValorAnual1() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2011"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2011 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2010"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2010 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual2() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2012"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2012 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual3() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2013"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2013 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual4() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2014"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2014 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual5() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2015"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2015 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual6() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2016"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2016 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual7() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2017"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2017 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual8() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2018"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2018 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarValorAnual9() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getValorDASHAnual.php?ano_selecionado=2019"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.valorTotal);
        this.valor2019 = getQuantidade.valorTotal;
      });
    this.serviceUrl.fecharLoding();
  }
}
