import { TestBed } from '@angular/core/testing';

import { GraficoAnualService } from './grafico-anual.service';

describe('GraficoAnualService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraficoAnualService = TestBed.get(GraficoAnualService);
    expect(service).toBeTruthy();
  });
});
