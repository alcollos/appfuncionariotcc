import { TestBed } from '@angular/core/testing';

import { GraficoDiarioService } from './grafico-diario.service';

describe('GraficoDiarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraficoDiarioService = TestBed.get(GraficoDiarioService);
    expect(service).toBeTruthy();
  });
});
