import { Injectable } from "@angular/core";
import { Component, ViewChild, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";

import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { UrlService } from "./url.service";
@Injectable({
  providedIn: "root"
})
export class GraficoDiarioService {
  barChart: any;
  doughnutChart: any;
  lineChart: any;

  qtdTotal: any;
  qtdMensal: any;
  qtdDiaria: any;
  qtdTempo: any;
  data_inicial: any;
  data_final: any;
  dataSelecionada: any;
  mesSelecionado: any;
  anoSelecionado: any;
  diaSelecionado: any;
  qtdAnual: any;
  qtdDiariaGrafico: any;
  @ViewChild("lineCanvas") lineCanvas;
  dia: FormGroup;

  qtdDia1: any;
  qtdDia2: any;
  qtdDia3: any;
  qtdDia4: any;
  qtdDia5: any;
  qtdDia6: any;
  qtdDia7: any;
  qtdDia8: any;
  qtdDia9: any;
  qtdDia10: any;
  qtdDia11: any;
  qtdDia12: any;
  qtdDia13: any;
  qtdDia14: any;
  qtdDia15: any;
  qtdDia16: any;
  qtdDia17: any;
  qtdDia18: any;
  qtdDia19: any;
  qtdDia20: any;
  qtdDia21: any;
  qtdDia22: any;
  qtdDia23: any;
  qtdDia24: any;
  qtdDia25: any;
  qtdDia26: any;
  qtdDia27: any;
  qtdDia28: any;
  qtdDia29: any;
  qtdDia30: any;
  qtdDia31: any;

  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router
  ) {
    this.dia = this.formBuilder.group({
      data_final: ["", Validators.required],
      data_inicial: ["", Validators.required],
      dataSelecionada: ["", Validators.required],
      mesSelecionado: ["", Validators.required],
      anoSelecionado: ["", Validators.required]
    });
  }
}
