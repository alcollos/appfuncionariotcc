import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MenuService {
  // Cria os espaços de opção do menu lateral
  public menu = [
    {
      label: "Adicionar novos produtos",
      icone: "icon ion-md-appstore",
      acao: "publicar",
      menu: "0",
      exibirMenu: true
    },
    {
      label: "Cadastro de Funcionários",
      icone: "icon ion-md-create",
      acao: "cadastrar-usuarios",
      menu: "1",
      exibirMenu: true
    },
    {
      label: "  Pedidos de Clientes",
      icone: "icon ion-md-contacts",
      acao: "selecionados",
      menu: "2",
      exibirMenu: true
    },
    {
      label: "Caixa",
      icone: "icon ion-md-basket",
      acao: "caixa",
      menu: "3",
      exibirMenu: true
    },
    {
      label: "Estoque",
      icone: "icon ion-md-list-box",
      acao: "estoque",
      menu: "4",
      exibirMenu: true
    },
    {
      label: "Dashboard",
      icone: "icon ion-md-pie",
      acao: "dashboardPrincipal",
      menu: "5",
      exibirMenu: true
    },
    {
      label: "Venda de Unidades",
      icone: "icon ion-md-podium",
      acao: "dashboardUnitario",
      menu: "6",
      exibirMenu: true
    },
    {
      label: "Satisfação dos Clientes",
      icone: "icon ion-md-happy",
      acao: "dashboardSatisfacao",
      menu: "7",
      exibirMenu: true
    }
  ];

  public perfilMenu = [];

  constructor() {}
}
