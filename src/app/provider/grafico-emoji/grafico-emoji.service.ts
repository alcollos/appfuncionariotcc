import { Injectable } from "@angular/core";
import { Component, ViewChild, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";
import { GraficoMensalService } from "src/app/provider/grafico-mensal/grafico-mensal.service";
import { GraficoAnualService } from "src/app/provider/grafico-anual/grafico-anual.service";

@Injectable({
  providedIn: "root"
})
export class GraficoEmojiService {
  emojiFeliz: any;
  emojiModerado: any;
  emojiRuim: any;
  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router
  ) {}

  scriptEmojiMensal1(anoSelecionado, mesSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getEmojiMensal.php?mes_selecionado=" +
          mesSelecionado +
          "&ano_selecionado=" +
          anoSelecionado +
          "&emoji=1"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiFeliz = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptEmojiMensal2(anoSelecionado, mesSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getEmojiMensal.php?mes_selecionado=" +
          mesSelecionado +
          "&ano_selecionado=" +
          anoSelecionado +
          "&emoji=2"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiModerado = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  scriptEmojiMensal3(anoSelecionado, mesSelecionado) {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getEmojiMensal.php?mes_selecionado=" +
          mesSelecionado +
          "&ano_selecionado=" +
          anoSelecionado +
          "&emoji=3"
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiRuim = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
}
