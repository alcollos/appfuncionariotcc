import { TestBed } from '@angular/core/testing';

import { GraficoEmojiService } from './grafico-emoji.service';

describe('GraficoEmojiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraficoEmojiService = TestBed.get(GraficoEmojiService);
    expect(service).toBeTruthy();
  });
});
