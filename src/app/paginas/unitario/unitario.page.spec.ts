import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitarioPage } from './unitario.page';

describe('UnitarioPage', () => {
  let component: UnitarioPage;
  let fixture: ComponentFixture<UnitarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitarioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
