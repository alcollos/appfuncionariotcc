import { Component, ViewChild, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";
import { GraficoMensalService } from "src/app/provider/grafico-mensal/grafico-mensal.service";
import { GraficoAnualService } from "src/app/provider/grafico-anual/grafico-anual.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { MenuService } from "src/app/provider/menu.service";

@Component({
  selector: "app-unitario",
  templateUrl: "./unitario.page.html",
  styleUrls: ["./unitario.page.scss"]
})
export class UnitarioPage implements OnInit {
  @ViewChild("lineCanvas") lineCanvas;

  valorJaneiro: any;
  valorFevereiro: any;
  valorMarco: any;
  valorAbril: any;
  valorMaio: any;
  valorJunho: any;
  valorSetembro: any;
  valorAgosto: any;
  valorOutubro: any;
  valorNovembro: any;
  valorDezembro: any;
  valorJulho: any;

  qtdJaneiro: any;
  qtdFevereiro: any;
  qtdMarço: any;
  qtdAbril: any;
  qtdMaio: any;
  qtdJunho: any;
  qtdJulho: any;
  qtdAgosto: any;
  qtdSetembro: any;
  qtdOutubro: any;
  qtdNovembro: any;
  qtdDezembro: any;
  lineChart: any;
  id: any;
  dia: FormGroup;
  anoSelecionado: any;

  qtdMensal: Array<{
    quantidade: any;
    valor: any;
    mes: any;
  }>;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    public formBuilder: FormBuilder,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,
    private ativeRouter: ActivatedRoute
  ) {
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
    });
    this.dia = this.formBuilder.group({
      anoSelecionado: ["", Validators.required]
    });

    console.log(this.id);
  }

  ngOnInit() {
    this.lineChartMethodMes();
  }

  scriptQuantidadeUnitario() {
    this.qtdMensal = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getTop10.php?ano=" +
          this.anoSelecionado +
          "&codProd=" +
          this.id
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.qtdMensal.push({
            quantidade: dados[i].quantidade,
            valor: dados[i].valor,
            mes: dados[i].mes
          });
          if (this.qtdMensal[i].mes === "1") {
            this.qtdJaneiro = this.qtdMensal[i].quantidade;
            this.valorJaneiro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "2") {
            this.qtdFevereiro = this.qtdMensal[i].quantidade;
            this.valorFevereiro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "3") {
            this.qtdMarço = this.qtdMensal[i].quantidade;
            this.valorMarco = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "4") {
            this.qtdAbril = this.qtdMensal[i].quantidade;

            this.valorAbril = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "5") {
            this.qtdMaio = this.qtdMensal[i].quantidade;
            this.valorMaio = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "6") {
            this.qtdJunho = this.qtdMensal[i].quantidade;
            this.valorJunho = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "7") {
            this.qtdJulho = this.qtdMensal[i].quantidade;
            this.valorJulho = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "8") {
            this.qtdAgosto = this.qtdMensal[i].quantidade;
            this.valorAgosto = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "9") {
            this.qtdSetembro = this.qtdMensal[i].quantidade;

            this.valorSetembro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "10") {
            this.qtdOutubro = this.qtdMensal[i].quantidade;
            this.valorOutubro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "11") {
            this.qtdNovembro = this.qtdMensal[i].quantidade;
            this.valorNovembro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "12") {
            this.qtdDezembro = this.qtdMensal[i].quantidade;

            this.valorDezembro = this.qtdMensal[i].valor;
          }
        }
      });
  }
  apresentarQuantidadeUnitario() {
    this.scriptQuantidadeUnitario();
    this.lineChart.data.datasets[0].data[0] = this.qtdJaneiro;
    this.lineChart.data.datasets[1].data[0] = this.valorJaneiro;

    this.lineChart.data.datasets[0].data[1] = this.qtdFevereiro;
    this.lineChart.data.datasets[1].data[1] = this.valorFevereiro;

    this.lineChart.data.datasets[0].data[2] = this.qtdMarço;

    this.lineChart.data.datasets[0].data[3] = this.qtdAbril;

    this.lineChart.data.datasets[0].data[4] = this.qtdMaio;

    this.lineChart.data.datasets[0].data[5] = this.qtdJunho;

    this.lineChart.data.datasets[0].data[6] = this.qtdJulho;

    this.lineChart.data.datasets[0].data[7] = this.qtdAgosto;

    this.lineChart.data.datasets[0].data[8] = this.qtdSetembro;

    this.lineChart.data.datasets[0].data[9] = this.qtdOutubro;

    this.lineChart.data.datasets[0].data[10] = this.qtdNovembro;

    this.lineChart.data.datasets[0].data[11] = this.qtdDezembro;

    this.lineChart.data.datasets[1].data[2] = this.valorMarco;
    this.lineChart.data.datasets[1].data[3] = this.valorAbril;
    this.lineChart.data.datasets[1].data[4] = this.valorMaio;
    this.lineChart.data.datasets[1].data[5] = this.valorJunho;
    this.lineChart.data.datasets[1].data[6] = this.valorJulho;
    this.lineChart.data.datasets[1].data[7] = this.valorAgosto;
    this.lineChart.data.datasets[1].data[8] = this.valorSetembro;
    this.lineChart.data.datasets[1].data[9] = this.valorOutubro;
    this.lineChart.data.datasets[1].data[10] = this.valorNovembro;
    this.lineChart.data.datasets[1].data[11] = this.valorDezembro;

    this.lineChart.update();
  }

  updateAll() {
    setInterval(() => {
      this.apresentarQuantidadeUnitario();
    }, 1000);
  }

  atualizarUnitario() {
    this.apresentarQuantidadeUnitario();
    this.updateAll();
    this.qtdJaneiro = 0;
    this.qtdFevereiro = 0;
    this.qtdMarço = 0;
    this.qtdAbril = 0;
    this.qtdMaio = 0;
    this.qtdJulho = 0;
    this.qtdJunho = 0;
    this.qtdAgosto = 0;
    this.qtdSetembro = 0;
    this.qtdOutubro = 0;
    this.qtdNovembro = 0;
    this.qtdDezembro = 0;

    this.valorJaneiro = 0;
    this.valorFevereiro = 0;
    this.valorMarco = 0;
    this.valorAbril = 0;
    this.valorMaio = 0;
    this.valorJunho = 0;
    this.valorJulho = 0;
    this.valorAgosto = 0;
    this.valorSetembro = 0;
    this.valorOutubro = 0;
    this.valorNovembro = 0;
    this.valorDezembro = 0;

    this.lineChart.update();
  }
  lineChartMethodMes() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: [
          "Janeiro",
          "Fevereiro",
          "Março",
          "Abril",
          "Maio",
          "Junho",
          "Julho",
          "Agosto",
          "Setembro",
          "Outubro",
          "Novembro",
          "Dezembro"
        ],
        datasets: [
          {
            label: "Quantidade de vendas feitas por mês",

            fill: false,
            lineTension: 0.1,
            backgroundColor: "#10dc60",
            borderColor: "#10dc60",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#10dc60",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 10,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              this.qtdJaneiro,
              this.qtdFevereiro,
              this.qtdMarço,
              this.qtdAbril,
              this.qtdMaio,
              this.qtdJulho,
              this.qtdJunho,
              this.qtdAgosto,
              this.qtdSetembro,
              this.qtdOutubro,
              this.qtdNovembro,
              this.qtdDezembro
            ],
            spanGaps: false
          },
          {
            label: "Valor vendido por mês R$",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#cae398",
            borderColor: "#cae398",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#cae398",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            data: [
              this.valorJaneiro,
              this.valorJaneiro,
              this.valorFevereiro,
              this.valorMarco,
              this.valorAbril,
              this.valorMaio,
              this.valorJunho,
              this.valorJulho,
              this.valorAgosto,
              this.valorSetembro,
              this.valorOutubro,
              this.valorNovembro,
              this.valorDezembro
            ],

            spanGaps: false
          }
        ]
      }
    });
  }
}
