import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { PostServiceService } from "src/app/provider/post/post-service.service";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-list-finalizacao",
  templateUrl: "./list-finalizacao.page.html",
  styleUrls: ["./list-finalizacao.page.scss"]
})
export class ListFinalizacaoPage implements OnInit {
  atualizarCliente: any; // formGroup
  atualizarQuantidade: any;
  // Variaveis do cliente
  nomeCliente: any;
  emailCliente: any;
  nascimentoCliente: any;
  cpfCliente: any;
  enderecoCliente: any;
  rgCliente: any;
  telefoneCliente: any;
  codigoCliente: any;

  id: any;
  detalhes: any;
  produtosCliente: Array<{
    quantidadeCarrinho;
    codigoCliente: any;
    codigoCarrinho: any;
    codigoProd: any;
    modelo: any;
    descricao: any;
    foto: any;
    categoria: any;
    marca: any;
    valor: any;
    nomeCliente: any;
    emailCliente: any;
    tamanho: any;
    cor: any;
    quantidadeProd: any;
    nascimentoCliente: any;
    cpfCliente: any;
    enderecoCliente: any;
    rgCliente: any;
    telefoneCliente: any;
    valorTotal: any;
  }>;

  valorTotal: Array<{
    codigoCliente: any;

    valorTotal: any;
  }>;
  quantidadeCarrinho: any;
  detalhe: any;
  teste: any;
  precoTotal: any;
  constructor(
    private http: Http,
    public urlService: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public postService: PostServiceService,
    public nav: NavController
  ) {
    this.listDetalhes();

    this.atualizarCliente = this.formBuilder.group({
      nomeCliente: ["", Validators.required],
      emailCliente: ["", Validators.required],
      nascimentoCliente: ["", Validators.required],
      cpfCliente: ["", Validators.required],
      enderecoCliente: ["", Validators.required],
      rgCliente: ["", Validators.required],
      telefoneCliente: ["", Validators.required],

      codigoCliente: [this.id, Validators.required]
    });

    this.atualizarQuantidade = this.formBuilder.group({
      quantidadeCarrinho: ["", Validators.required]
    });
    this.listCompraTotal();
  }
  listDetalhes() {
    this.produtosCliente = [];
    let oi;
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
      oi = this.id;

      this.http
        .get(
          this.urlService.getUrlEstoque() + "detalhesCompra.php?idcliente=" + oi
        )
        .pipe(map(res => res.json()))
        .subscribe(data => {
          for (let i = 0; i < data.length; i++) {
            this.produtosCliente.push({
              codigoCliente: data[i]["codigoCliente"],
              codigoProd: data[i]["codigoProd"],
              codigoCarrinho: data[i]["codigoCarrinho"],
              modelo: data[i]["modelo"],
              descricao: data[i]["descricao"],
              foto: data[i]["foto"],
              categoria: data[i]["categoria"],
              marca: data[i]["marca"],
              valor: data[i]["valor"],
              tamanho: data[i]["tamanho"],
              cor: data[i]["cor"],
              quantidadeProd: data[i]["quantidadeProd"],
              quantidadeCarrinho: data[i]["quantidadeCarrinho"],
              valorTotal: data[i].valorTotal,
              // Dados dos clientes
              nomeCliente: data[i]["nomeCliente"],
              emailCliente: data[i]["emailCliente"],
              nascimentoCliente: data[i]["nascimentoCliente"],
              cpfCliente: data[i]["cpfCliente"],
              enderecoCliente: data[i]["enderecoCliente"],
              rgCliente: data[i]["rgCliente"],
              telefoneCliente: data[i]["telefoneCliente"]
            });
          }

          this.nomeCliente = this.produtosCliente[0].nomeCliente;
          this.emailCliente = this.produtosCliente[0].emailCliente;
          this.nascimentoCliente = this.produtosCliente[0].nascimentoCliente;
          this.cpfCliente = this.produtosCliente[0].cpfCliente;
          this.enderecoCliente = this.produtosCliente[0].enderecoCliente;
          this.rgCliente = this.produtosCliente[0].rgCliente;
          this.telefoneCliente = this.produtosCliente[0].telefoneCliente;
          this.codigoCliente = this.produtosCliente[0].codigoCliente;
        });
    });
  }

  listCompraTotal() {
    this.valorTotal = [];
    let oi;
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
      oi = this.id;

      this.http
        .get(
          this.urlService.getUrlEstoque() +
            "detalhesCompraValorTotal.php?idcliente=" +
            oi
        )
        .pipe(map(res => res.json()))
        .subscribe(data => {
          for (let i = 0; i < data.length; i++) {
            this.valorTotal.push({
              codigoCliente: data[i]["codigoCliente"],

              valorTotal: data[i].valorCompra
            });
          }
          this.precoTotal = this.valorTotal[0].valorTotal;
          console.log(this.precoTotal);
        });
    });
  }

  postData(valor) {
    console.log("inserindo");
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "updateCliente.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  atualizarQuantidadeProdutos(produto) {
    console.log(this.quantidadeCarrinho);
    produto.quantidadeCarrinho = this.quantidadeCarrinho;

    this.postService.postEditQuantidadeCarrinho(produto).subscribe(data => {});
  }
  atualizarDadosCliente() {
    if (
      this.nomeCliente == undefined ||
      this.emailCliente == undefined ||
      this.rgCliente == undefined ||
      this.cpfCliente == undefined ||
      this.telefoneCliente == undefined ||
      this.enderecoCliente == undefined ||
      this.nascimentoCliente == undefined
    ) {
      this.urlService.alertas("Atenção", "Preencha todos os Campos");
    } else {
      this.postData(this.atualizarCliente.value).subscribe(data => {
        this.nav.navigateForward("home");
        console.log("Inserido com sucesso");
      });
    }
  }

  deleteCompra() {
    this.postService.deleteCompras(this.codigoCliente).subscribe(
      data => {
        console.log("produto deletado com sucesso");
      },
      error => {
        console.log("erro ao tentar excluir" + error);
      }
    );
  }

  realizarVenda() {
    this.deleteCompra();

    this.detalhe = {
      queryParams: {
        codCliente: this.produtosCliente[0].codigoCliente
      }
    };

    this.route.navigate(["baixa-quantidade"], this.detalhe);
  }

  ngOnInit() {}
}
