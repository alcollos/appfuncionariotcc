import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListFinalizacaoPage } from './list-finalizacao.page';

const routes: Routes = [
  {
    path: '',
    component: ListFinalizacaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListFinalizacaoPage]
})
export class ListFinalizacaoPageModule {}
