import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProdutosPage } from './edit-produtos.page';

describe('EditProdutosPage', () => {
  let component: EditProdutosPage;
  let fixture: ComponentFixture<EditProdutosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProdutosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProdutosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
