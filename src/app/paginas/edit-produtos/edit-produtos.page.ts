import { Component, OnInit, ɵConsole } from "@angular/core";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../../provider/url.service";
import {
  NavController,
  Platform,
  LoadingController,
  ToastController,
  ActionSheetController
} from "@ionic/angular";
import { ServiceUserService } from "../../provider/service-user.service";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ActivatedRoute } from "@angular/router";

import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { PostServiceService } from "src/app/provider/post/post-service.service";

@Component({
  selector: "app-edit-produtos",
  templateUrl: "./edit-produtos.page.html",
  styleUrls: ["./edit-produtos.page.scss"]
})
export class EditProdutosPage implements OnInit {
  codProd: any;
  qtd: any;
  valor: any;
  descricao: any;
  codTamanho: any;
  tamanho: any;
  codCor: any;
  cor: any;
  codQtd: any;
  quantidade: any;
  codCateg: any;
  categorias: any;
  codFornecedor: any;
  fornecedores: any;
  codMarca: any;
  marcas: any;
  modelos: any;
  codPalmilha: any;
  palmilhas: any;
  codPeso: any;
  pesos: any;
  codPromo: any;
  promocoes: any;

  // Variaveis utilizadas para captar qual o valor que foi selecionado no select
  corSelecionado: any;
  quantidadeSelecionado: any;
  tamanhoSelecionado: any;
  categoriaSelecionado: any;
  fornecedorSelecionado: any;
  marcaSelecionado: any;
  pesoSelecionado: any;
  palmilhaSelecionado: any;
  promocaoSelecionado: any;

  aCateg: Array<{
    cod_categ: any;
    tipo_categ: any;
  }>;

  aCor: Array<{
    cod_cor: any;
    nome: any;
  }>;

  aQuantidade: Array<{
    cod_qtd: any;
    quantidade: any;
  }>;

  aFornecedor: Array<{
    cod_fornecedor: any;
    nome_fornecedor: any;
  }>;

  aMarca: Array<{
    cod_marca: any;
    nome_marca: any;
  }>;

  aPalmilha: Array<{
    cod_palmilha: any;
    tipo_palmilha: any;
  }>;

  aPeso: Array<{
    cod_peso: any;
    peso: any;
  }>;

  aTamanho: Array<{
    cod_tamanho: any;
    tamanho: any;
  }>;

  aPromocao: Array<{
    cod_promocao: any;
    nome_promocao: any;
  }>;

  postagem: any;
  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService,
    public serviceUser: ServiceUserService,
    public nfce: NFC,
    public postService: PostServiceService,

    // tslint:disable-next-line: max-line-length
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public ndef: Ndef
  ) {
    this.activedRoute.queryParams.subscribe(parametros => {
      this.codProd = parametros["codProd"];
      this.codQtd = parametros["codQtd"];
      this.qtd = parametros["quantidade"];
      this.valor = parametros["valor"];
      this.descricao = parametros["descricao"];
      this.codCor = parametros["codCor"];
      this.cor = parametros["cor"];
      this.codCateg = parametros["codCateg"];
      this.categorias = parametros["categoria"];
      this.codFornecedor = parametros["codFornecedor"];
      this.fornecedores = parametros["fornecedor"];
      this.codMarca = parametros["codMarca"];
      this.marcas = parametros["marca"];
      this.modelos = parametros["modelo"];
      this.codPalmilha = parametros["codPalmilha"];
      this.palmilhas = parametros["palmilha"];
      this.codPeso = parametros["codPeso"];
      this.pesos = parametros["peso"];
      this.codTamanho = parametros["codTamanho"];
      this.tamanho = parametros["tamanho"];
      this.codPromo = parametros["codPromo"];
      this.promocoes = parametros["promocao"];

      console.log(this.codProd);
      this.listCor();
      this.listCategoria();
      this.listFornecedor();
      this.listMarca();
      this.listPalmilha();
      this.listPeso();
      this.listTamanho();
      this.listPromocao();
      this.listQuantidade();

      this.postagem = this.formBuilder.group({
        valor: ["", Validators.required],
        qtd: ["", Validators.required],
        descricao: ["", Validators.required],
        cores: ["", Validators.required],
        categorias: ["", Validators.required],
        fornecedores: ["", Validators.required],
        marcas: ["", Validators.required],
        modelos: ["", Validators.required],
        palmilhas: ["", Validators.required],
        pesos: ["", Validators.required],
        tamanhos: ["", Validators.required],
        promocoes: ["", Validators.required],
        codProd: [this.codProd, Validators.required]
      });
    });
  }

  listCor() {
    this.aCor = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listCor.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aCor.push({
            cod_cor: dados[i]["codigo"],
            nome: dados[i]["cor"]
          });
        }
      });
  }

  listCategoria() {
    this.aCateg = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listCategoria.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aCateg.push({
            cod_categ: dados[i]["codigo"],
            tipo_categ: dados[i]["tipo"]
          });
        }
      });
  }

  listFornecedor() {
    this.aFornecedor = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listFornecedor.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aFornecedor.push({
            cod_fornecedor: dados[i]["codigo"],
            nome_fornecedor: dados[i]["nome"]
          });
        }
      });
  }

  listMarca() {
    this.aMarca = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listMarca.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aMarca.push({
            cod_marca: dados[i]["codigo"],
            nome_marca: dados[i]["nome"]
          });
        }
      });
  }

  listPalmilha() {
    this.aPalmilha = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listPalmilha.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aPalmilha.push({
            cod_palmilha: dados[i]["codigo"],
            tipo_palmilha: dados[i]["tipo"]
          });
        }
      });
  }

  listPeso() {
    this.aPeso = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listPeso.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aPeso.push({
            cod_peso: dados[i]["codigo"],
            peso: dados[i]["peso"]
          });
        }
      });
  }

  listTamanho() {
    this.aTamanho = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listTamanho.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aTamanho.push({
            cod_tamanho: dados[i]["codigo"],
            tamanho: dados[i]["tamanho"]
          });
        }
      });
  }

  listPromocao() {
    this.aPromocao = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listPromocao.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aPromocao.push({
            cod_promocao: dados[i]["codigo"],
            nome_promocao: dados[i]["nome"]
          });
        }
      });
  }

  listQuantidade() {
    this.aQuantidade = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listQuantidade.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aQuantidade.push({
            cod_qtd: dados[i]["codigo"],
            quantidade: dados[i]["quantidade"]
          });
        }
      });
  }

  nfc() {
    this.nfce
      .addNdefListener(
        () => {
          console.log("successfully attached ndef listener");
        },
        err => {
          console.log("error attaching ndef listener", err);
        }
      )
      .subscribe(event => {
        const message = [this.ndef.textRecord(this.codProd)];
        this.nfce.write(message);
      });
  }

  postarProduto() {
    console.log(this.postagem.value);

    if (
      this.descricao === undefined ||
      this.modelos === undefined ||
      this.valor === undefined
    ) {
      this.urlService.alertas("Atenção", "Preencha todos os Campos");
      console.log(this.valor);
    } else {
      this.postService.postEdit(this.postagem.value).subscribe(data => {});
      this.urlService.alertas(
        "Atenção !!",
        "O produto: " + this.modelos + " foi editado."
      );
    }
  }
  ngOnInit() {}
}
