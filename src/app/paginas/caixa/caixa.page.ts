import { Component, OnInit } from "@angular/core";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { NavController, NavParams } from "@ionic/angular";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ServiceUserService } from "../../provider/service-user.service";
import { MenuService } from "../../provider/menu.service";

@Component({
  selector: "app-caixa",
  templateUrl: "./caixa.page.html",
  styleUrls: ["./caixa.page.scss"]
})
export class CaixaPage implements OnInit {
  cliente: Array<{
    foto: any;
    nome: any;
    telefone: any;
    codigoCliente: any;
  }>;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService
  ) {
    this.listClientes();
  }
  listClientes() {
    this.cliente = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrlCarrinho() + "listCompra.php")
      .pipe(map(res => res.json()))
      .subscribe(dadosCliente => {
        for (let i = 0; i < dadosCliente.length; i++) {
          this.cliente.push({
            foto: dadosCliente[i]["foto"],
            nome: dadosCliente[i]["nome"],
            telefone: dadosCliente[i]["telefone"],
            codigoCliente: dadosCliente[i]["codigo"]
          });
        }
        this.serviceUrl.fecharLoding();
        console.log(this.cliente[0].foto);
      });
    this.serviceUrl.fecharLoding();
  }
  acessarCompra(clientes) {
    this.nav.navigateForward("list-finalizacao/" + clientes.codigoCliente);
  }
  doRefresh(event) {
    console.log("Begin async operation");
    this.listClientes();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }
  ngOnInit() {}
}
