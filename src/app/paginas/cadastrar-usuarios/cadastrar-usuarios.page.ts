import { Component, OnInit } from "@angular/core";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../../provider/url.service";
import { NavController } from "@ionic/angular";
import { ServiceUserService } from "../../provider/service-user.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-cadastrar-usuarios",
  templateUrl: "./cadastrar-usuarios.page.html",
  styleUrls: ["./cadastrar-usuarios.page.scss"]
})
export class CadastrarUsuariosPage implements OnInit {
  cadastrar: any;
  nome: any;
  email: any;
  senha: any;
  nivel: any;
  rg: any;
  cpf: any;
  telefone: any;
  endereco: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService
  ) {
    this.cadastrar = this.formBuilder.group({
      nome: ["", Validators.required],
      email: ["", Validators.required],
      senha: ["", Validators.required],
      nivel: ["", Validators.required],
      rg: ["", Validators.required],
      cpf: ["", Validators.required],
      telefone: ["", Validators.required],
      endereco: ["", Validators.required]
    });
  }

  cadastrarUsuario() {
    console.log(this.nome);
    if (
      this.nome === undefined ||
      this.email === undefined ||
      this.senha === undefined ||
      this.nivel === undefined ||
      this.rg === undefined ||
      this.cpf === undefined ||
      this.telefone === undefined ||
      this.endereco === undefined
    ) {
      this.urlService.alertas("Atenção", "Preencha todos os Campos");
    } else {
      this.postData(this.cadastrar.value).subscribe(data => {});
      this.urlService.alertas(
        "Atenção ",
        "O funcionário: " + this.nome + "foi cadastrado."
      );
    }
  }

  postData(valor) {
    console.log(this.senha);

    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlCadastro() + "cadastrarFuncionario.php",
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
  ngOnInit() {}
}
