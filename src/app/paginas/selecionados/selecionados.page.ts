import { Component, OnInit } from "@angular/core";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { NavController, NavParams } from "@ionic/angular";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ServiceUserService } from "../../provider/service-user.service";
import { MenuService } from "../../provider/menu.service";
import { jsonpFactory } from "@angular/http/src/http_module";

@Component({
  selector: "app-selecionados",
  templateUrl: "./selecionados.page.html",
  styleUrls: ["./selecionados.page.scss"]
})
export class SelecionadosPage implements OnInit {
  cliente: Array<{
    foto: any;
    nome: any;
    telefone: any;
    codigoCliente: any;
  }>;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService
  ) {
    this.listClientes();
  }

  listClientes() {
    this.cliente = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrlCarrinho() + "listcarrinhoCliente.php")
      .pipe(map(res => res.json()))
      .subscribe(dadosCliente => {
        for (let i = 0; i < dadosCliente.length; i++) {
          this.cliente.push({
            foto: dadosCliente[i]["foto"],
            nome: dadosCliente[i]["nome"],
            telefone: dadosCliente[i]["telefone"],
            codigoCliente: dadosCliente[i]["codigo"]
          });
        }
        this.serviceUrl.fecharLoding();
        console.log(this.cliente[0].foto);
      });
    this.serviceUrl.fecharLoding();
  }
  mandarCliente(clientes) {
    this.nav.navigateForward("list-selecionados/" + clientes.codigoCliente);
  }
  ngOnInit() {}
  doRefresh(event) {
    console.log("Begin async operation");
    this.listClientes();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }
}
