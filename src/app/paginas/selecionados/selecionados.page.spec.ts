import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecionadosPage } from './selecionados.page';

describe('SelecionadosPage', () => {
  let component: SelecionadosPage;
  let fixture: ComponentFixture<SelecionadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecionadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecionadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
