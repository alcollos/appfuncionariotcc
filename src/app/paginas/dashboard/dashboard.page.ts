import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"]
})
export class DashboardPage implements OnInit {
  qtdTotal: any;
  qtdMensal: any;
  qtdDiaria: any;
  qtdTempo: any;
  data_inicial: any;
  data_final: any;
  dataSelecionada: any;
  mesSelecionado: any;
  anoSelecionado: any;
  qtdAnual: any;

  dia: FormGroup;

  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router
  ) {
    this.apresentarQuantidade();
    this.dia = this.formBuilder.group({
      data_final: ["", Validators.required],
      data_inicial: ["", Validators.required],
      dataSelecionada: ["", Validators.required],
      mesSelecionado: ["", Validators.required],
      anoSelecionado: ["", Validators.required]
    });
  }

  ngOnInit() {}

  apresentarQuantidade() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrlEstoque() + "getQuantidadeDASH.php")
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdTotal = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarQuantidadeTempo() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHTempo.php?data_inicial=" +
          this.data_inicial +
          "&data_final=" +
          this.data_final
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdTempo = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarQuantidadeDiaria() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHDiaria.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdDiaria = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarQuantidadeMensal() {
    console.log(this.anoSelecionado);
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHMensal.php?mes_selecionado=" +
          this.mesSelecionado +
          "&ano_selecionado=" +
          this.anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdMensal = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarQuantidadeAnual() {
    console.log(this.anoSelecionado);
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHAnual.php?ano_selecionado=" +
          this.anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdAnual = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
}
