import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { PostServiceService } from "src/app/provider/post/post-service.service";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-baixa-quantidade",
  templateUrl: "./baixa-quantidade.page.html",
  styleUrls: ["./baixa-quantidade.page.scss"]
})
export class BaixaQuantidadePage implements OnInit {
  atualizarQuantidade: any;

  id: any;
  detalhes: any;
  produtosCliente: Array<{
    quantidadeCarrinho: any;
    codigoCliente: any;
    codigoCarrinho: any;
    codigoProd: any;
    modelo: any;
    descricao: any;
    foto: any;
    categoria: any;
    marca: any;
    valor: any;

    tamanho: any;
    cor: any;
    quantidadeProd: any;
  }>;
  quantidadeCarrinho: any;
  codCliente: any;
  data: Date;

  codigoProduto: any;

  constructor(
    private http: Http,
    public urlService: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    public postService: PostServiceService,
    public nav: NavController
  ) {
    this.atualizarQuantidade = this.formBuilder.group({
      quantidadeCarrinho: ["", Validators.required]
    });

    this.ativeRouter.queryParams.subscribe(parametros => {
      this.codCliente = parametros["codCliente"];
    });
    this.listDetalhes();
  }

  ngOnInit() {}

  voltarEstoque(produtos, event) {
    this.postService.deleteNotaFiscal(produtos).subscribe(data => {});
    this.postService.deleteCarAll(produtos).subscribe(data => {});
    this.postService.postEditQuantidade(produtos).subscribe(data => {});
    this.urlService.alertas(
      "Atenção",
      produtos.quantidadeCarrinho +
        " produtos do modelo: " +
        produtos.modelo +
        " foi retirado do estoque"
    );

    this.postService.postVerificarQuantidade(produtos).subscribe(dados => {
      if (dados.msg.produto === "existe") {
        this.postService
          .adicionarQuantidadeVenda(produtos)
          .subscribe(data => {});
      } else {
        this.postService.postVendaFeita(produtos).subscribe(data => {});
      }
    });
    this.doRefresh(event);
  }

  atualizarQuantidadeProdutos(produto) {
    console.log(this.quantidadeCarrinho);
    produto.quantidadeCarrinho = this.quantidadeCarrinho;

    this.postService.postEditQuantidadeCarrinho(produto).subscribe(data => {});
  }

  listDetalhes() {
    this.produtosCliente = [];

    console.log(this.codCliente);
    this.http
      .get(
        this.urlService.getUrlEstoque() +
          "detalhesCompra.php?idcliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.produtosCliente.push({
            codigoCliente: data[i]["codigoCliente"],
            codigoProd: data[i]["codigoProd"],
            codigoCarrinho: data[i]["codigoCarrinho"],
            modelo: data[i]["modelo"],
            descricao: data[i]["descricao"],
            foto: data[i]["foto"],
            categoria: data[i]["categoria"],
            marca: data[i]["marca"],
            valor: data[i]["valor"],
            tamanho: data[i]["tamanho"],
            cor: data[i]["cor"],

            quantidadeProd: data[i]["quantidadeProd"],
            quantidadeCarrinho: data[i]["quantidadeCarrinho"]
          });
          this.codigoProduto = this.produtosCliente[i].codigoProd;
        }
      });
  }

  voltarHome() {
    this.nav.navigateForward("home");
  }

  doRefresh(event) {
    console.log("Begin async operation");
    this.listDetalhes();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }
}
