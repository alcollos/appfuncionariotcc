import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaixaQuantidadePage } from './baixa-quantidade.page';

describe('BaixaQuantidadePage', () => {
  let component: BaixaQuantidadePage;
  let fixture: ComponentFixture<BaixaQuantidadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaixaQuantidadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaixaQuantidadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
