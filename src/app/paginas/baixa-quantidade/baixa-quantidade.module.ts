import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { BaixaQuantidadePage } from "./baixa-quantidade.page";

const routes: Routes = [
  {
    path: "",
    component: BaixaQuantidadePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BaixaQuantidadePage]
})
export class BaixaQuantidadePageModule {}
