import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";
import { Validators } from "@angular/forms";
import { NavController } from "@ionic/angular";
import { Headers, Response, ResponseOptions } from "@angular/http";
@Component({
  selector: "app-list-selecionados",
  templateUrl: "./list-selecionados.page.html",
  styleUrls: ["./list-selecionados.page.scss"]
})
export class ListSelecionadosPage implements OnInit {
  id: any;
  detalhes: any;
  nome: any;
  codCliente: any;
  produtosCliente: Array<{
    codigo: any;
    nome: any;
    modelo: any;
    descricao: any;
    foto: any;
    categoria: any;
    marca: any;
    cor: any;
    quantidade: any;
    tamanho: any;
    codCliente: any;
  }>;
  entrega: string;
  constructor(
    private http: Http,
    public urlService: UrlService,
    private ativeRouter: ActivatedRoute,
    public nav: NavController
  ) {
    this.listDetalhes();
  }
  listDetalhes() {
    this.produtosCliente = [];
    let oi;
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
      oi = this.id;
      console.log(oi);

      this.http
        .get(
          this.urlService.getUrlEstoque() +
            "detalhesSelecionados.php?idcliente=" +
            oi
        )
        .pipe(map(res => res.json()))
        .subscribe(data => {
          for (let i = 0; i < data.length; i++) {
            this.produtosCliente.push({
              codigo: data[i]["codigo"],
              nome: data[i]["nome"],
              modelo: data[i]["modelo"],
              descricao: data[i]["descricao"],
              foto: data[i]["foto"],
              categoria: data[i]["categoria"],
              marca: data[i]["marca"],
              tamanho: data[i]["tamanho"],
              quantidade: data[i]["quantidade"],
              cor: data[i]["cor"],
              codCliente: data[i]["codCliente"]
            });
            console.log(this.produtosCliente);
            this.nome = this.produtosCliente[0].nome;
            this.codCliente = this.produtosCliente[0].codCliente;
          }
        });
    });
  }

  entregar() {
    this.alterarStatus();
    this.nav.navigateForward("home");
  }
  doRefresh(event) {
    console.log("Begin async operation");
    this.listDetalhes();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  postData(valor) {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlCadastro() +
          "alterarEntrega.php?codCliente=" +
          this.codCliente,
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  alterarStatus() {
    this.entrega = "sim";
    this.postData(this.entrega).subscribe(data => {});
  }

  ngOnInit() {}
}
