import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSelecionadosPage } from './list-selecionados.page';

describe('ListSelecionadosPage', () => {
  let component: ListSelecionadosPage;
  let fixture: ComponentFixture<ListSelecionadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSelecionadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSelecionadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
