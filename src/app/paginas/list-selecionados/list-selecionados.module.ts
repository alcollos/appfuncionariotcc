import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListSelecionadosPage } from './list-selecionados.page';

const routes: Routes = [
  {
    path: '',
    component: ListSelecionadosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListSelecionadosPage]
})
export class ListSelecionadosPageModule {}
