import { Component, ViewChild, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";
import { GraficoMensalService } from "src/app/provider/grafico-mensal/grafico-mensal.service";
import { GraficoAnualService } from "src/app/provider/grafico-anual/grafico-anual.service";
import { GraficoEmojiService } from "src/app/provider/grafico-emoji/grafico-emoji.service";

@Component({
  selector: "app-dashboard-satisfacao",
  templateUrl: "./dashboard-satisfacao.page.html",
  styleUrls: ["./dashboard-satisfacao.page.scss"]
})
export class DashboardSatisfacaoPage implements OnInit {
  @ViewChild("lineCanvas") lineCanvas;
  @ViewChild("barCanvas") barCanvas;

  lineChart: any;
  anoSelecionado: any;
  mesSelecionado: any;
  data: any;
  barChart: any;

  mediaJaneiroNPS: any;
  mediaFevereiroNPS: any;
  mediaMarcoNPS: any;
  mediaAbrilNPS: any;
  mediaMaioNPS: any;
  mediaJunhoNPS: any;
  mediaJulhoNPS: any;
  mediaAgostoNPS: any;
  mediaSetembroNPS: any;
  mediaOutubroNPS: any;
  mediaNovembroNPS: any;
  mediaDezembroNPS: any;

  emojiFeliz: any;
  emojiModerado: any;
  emojiTriste: any;

  mediaMensalNPS: Array<{
    mediaNota: any;
    mes: any;
  }>;

  mediaMensalEmoji: Array<{
    mediaEmoji: any;
    mes: any;
    ano: any;
    emoji: any;
  }>;
  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public graficoDiario: GraficoDiarioService,
    public graficoMensal: GraficoMensalService,
    public graficoAnual: GraficoAnualService,
    public graficoEmoji: GraficoEmojiService
  ) {
    this.data = this.formBuilder.group({
      anoSelecionado: ["", Validators.required],
      mesSelecionado: ["", Validators.required]
    });

    let data = new Date();
    this.anoSelecionado = data.getFullYear();
    this.mesSelecionado = data.getMonth();
  }

  ngOnInit() {
    this.lineChartMethodNPS();
    this.barChartMethod();
  }

  barChartMethod() {
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: ["Feliz", "Moderado", "Ruim"],
        datasets: [
          {
            label: "# of Votes",
            data: [this.emojiFeliz, this.emojiModerado, this.emojiTriste],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(255,99,132,1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });
  }

  apresentarGraficoEmoji() {
    console.log(this.emojiFeliz, this.emojiModerado, this.emojiTriste);
    this.scriptQuantidadeEmoji();
    this.barChart.data.datasets[0].data[0] = this.emojiFeliz;
    this.barChart.data.datasets[0].data[1] = this.emojiModerado;
    this.barChart.data.datasets[0].data[2] = this.emojiTriste;
    this.barChart.update();
  }
  scriptQuantidadeEmoji() {
    this.mediaMensalEmoji = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getEmojiMensal.php?mesSelecionado=" +
          this.mesSelecionado +
          "&anoSelecionado=" +
          this.anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.mediaMensalEmoji.push({
            mediaEmoji: dados[i].media,
            ano: dados[i].ano,
            mes: dados[i].mes,
            emoji: dados[i].emoji
          });

          if (this.mediaMensalEmoji[i].emoji === "1") {
            this.emojiFeliz = this.mediaMensalEmoji[i].mediaEmoji;
          }
          if (this.mediaMensalEmoji[i].emoji === "2") {
            this.emojiModerado = this.mediaMensalEmoji[i].mediaEmoji;
          }
          if (this.mediaMensalEmoji[i].emoji === "3") {
            this.emojiTriste = this.mediaMensalEmoji[i].mediaEmoji;
          }
        }
      });
  }

  // Gráfico de Linhas - Métrica  do NPS (NOTAS média.Mensal)
  lineChartMethodNPS() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: [
          "Janeiro",
          "Fevereiro",
          "Março",
          "Abril",
          "Maio",
          "Junho",
          "Julho",
          "Agosto",
          "Setembro",
          "Outubro",
          "Novembro",
          "Dezembro"
        ],
        datasets: [
          {
            label: "Média de Indicações mensais",

            fill: false,
            lineTension: 0.1,
            backgroundColor: "#10dc60",
            borderColor: "#10dc60",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#10dc60",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 10,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              this.mediaJaneiroNPS,
              this.mediaFevereiroNPS,
              this.mediaMarcoNPS,
              this.mediaAbrilNPS,
              this.mediaMaioNPS,
              this.mediaJulhoNPS,
              this.mediaJunhoNPS,
              this.mediaAgostoNPS,
              this.mediaSetembroNPS,
              this.mediaOutubroNPS,
              this.mediaNovembroNPS,
              this.mediaDezembroNPS
            ],
            spanGaps: false
          }
        ]
      }
    });
  }

  scriptQuantidadeNPS() {
    this.mediaMensalNPS = [];

    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getMediaDASHMensal.php?anoSelecionado=" +
          this.anoSelecionado
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.mediaMensalNPS.push({
            mediaNota: dados[i].media,
            mes: dados[i].mes
          });
          console.log(
            "mes" +
              this.mediaMensalNPS[i].mes +
              "media" +
              this.mediaMensalNPS[i].mediaNota
          );
          if (this.mediaMensalNPS[i].mes === "1") {
            this.mediaJaneiroNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "2") {
            this.mediaFevereiroNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "3") {
            this.mediaMarcoNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "4") {
            this.mediaAbrilNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "5") {
            this.mediaMaioNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "6") {
            this.mediaJunhoNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "7") {
            this.mediaJulhoNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "8") {
            this.mediaAgostoNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "9") {
            this.mediaSetembroNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "10") {
            this.mediaOutubroNPS = this.mediaMensalNPS[i].mediaNota;
          }
          if (this.mediaMensalNPS[i].mes === "11") {
            this.mediaNovembroNPS = this.mediaMensalNPS[i].mediaNota;
          }

          if (this.mediaMensalNPS[i].mes === "12") {
            this.mediaDezembroNPS = this.mediaMensalNPS[i].mediaNota;
          }
          /*this.mediaFevereiroNPS = this.mediaMensalNPS[1].mediaNota;
          this.mediaMarcoNPS = this.mediaMensalNPS[2].mediaNota;
          this.mediaAbrilNPS = this.mediaMensalNPS[3].mediaNota;
          this.mediaMaioNPS = this.mediaMensalNPS[4].mediaNota;
          this.mediaJulhoNPS = this.mediaMensalNPS[5].mediaNota;
          this.mediaJunhoNPS = this.mediaMensalNPS[6].mediaNota;
          this.mediaAgostoNPS = this.mediaMensalNPS[7].mediaNota;
          this.mediaSetembroNPS = this.mediaMensalNPS[8].mediaNota;
          this.mediaOutubroNPS = this.mediaMensalNPS[9].mediaNota;
          this.mediaNovembroNPS = this.mediaMensalNPS[10].mediaNota;
          this.mediaDezembroNPS = this.mediaMensalNPS[11].mediaNota;*/
          console.log(this.mediaJaneiroNPS, this.mediaDezembroNPS);
        }
      });
  }

  apresentarQuantidadeNPS() {
    console.log(
      this.mediaJaneiroNPS,
      this.mediaFevereiroNPS,
      this.mediaMarcoNPS,
      this.mediaAbrilNPS,
      this.mediaMaioNPS,
      this.mediaJunhoNPS,
      this.mediaJulhoNPS,
      this.mediaAgostoNPS,
      this.mediaSetembroNPS,
      this.mediaOutubroNPS,
      this.mediaNovembroNPS,
      this.mediaDezembroNPS
    );
    this.scriptQuantidadeNPS();
    this.lineChart.data.datasets[0].data[0] = this.mediaJaneiroNPS;
    this.lineChart.data.datasets[0].data[1] = this.mediaFevereiroNPS;
    this.lineChart.data.datasets[0].data[2] = this.mediaMarcoNPS;
    this.lineChart.data.datasets[0].data[3] = this.mediaAbrilNPS;
    this.lineChart.data.datasets[0].data[4] = this.mediaMaioNPS;
    this.lineChart.data.datasets[0].data[5] = this.mediaJunhoNPS;
    this.lineChart.data.datasets[0].data[6] = this.mediaJulhoNPS;
    this.lineChart.data.datasets[0].data[7] = this.mediaAgostoNPS;
    this.lineChart.data.datasets[0].data[8] = this.mediaSetembroNPS;
    this.lineChart.data.datasets[0].data[9] = this.mediaOutubroNPS;
    this.lineChart.data.datasets[0].data[10] = this.mediaNovembroNPS;
    this.lineChart.data.datasets[0].data[11] = this.mediaDezembroNPS;
    this.lineChart.update();
  }

  atualizarDashboardNPS() {
    this.updateAll();
    this.mediaJaneiroNPS = 0;
    this.mediaFevereiroNPS = 0;
    this.mediaMarcoNPS = 0;
    this.mediaAbrilNPS = 0;
    this.mediaMaioNPS = 0;
    this.mediaJunhoNPS = 0;
    this.mediaJulhoNPS = 0;
    this.mediaAgostoNPS = 0;
    this.mediaSetembroNPS = 0;
    this.mediaOutubroNPS = 0;
    this.mediaNovembroNPS = 0;
    this.mediaDezembroNPS = 0;

    this.emojiFeliz = 0;
    this.emojiModerado = 0;
    this.emojiTriste = 0;
    this.apresentarQuantidadeNPS();
  }
  updateAll() {
    setInterval(() => {
      this.apresentarGraficoEmoji();
      this.apresentarQuantidadeNPS();
    }, 1000);
    console.log(this.data);
  }
}
