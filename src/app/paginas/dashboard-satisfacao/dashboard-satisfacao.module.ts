import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DashboardSatisfacaoPage } from "./dashboard-satisfacao.page";

const routes: Routes = [
  {
    path: "",
    component: DashboardSatisfacaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardSatisfacaoPage]
})
export class DashboardSatisfacaoPageModule {}
