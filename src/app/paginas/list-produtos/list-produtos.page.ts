import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-list-produtos',
  templateUrl: './list-produtos.page.html',
  styleUrls: ['./list-produtos.page.scss']
})
export class ListProdutosPage implements OnInit {
  id: any;
  detalhes: any;
  dados: Array<{ codigo: any; nome: any; valor: any }>;

  constructor(
    private http: Http,
    private urlService: UrlService,
    private ativeRouter: ActivatedRoute
  ) {
    this.listDetalhes();
    this.dados = [];
  }

  listDetalhes() {
    let oi;
    this.ativeRouter.params.subscribe(paramsId => {
      this.id = paramsId.id;
      oi = this.id;
      console.log(oi);

      this.http
        .get(this.urlService.getUrlEstoque() + 'detalhesProdutos.php?idproduto=' + oi)
        .pipe(map(res => res.json()))
        .subscribe(data => {
          for (let i = 0; i < data.length; i++) {
            this.dados.push({
              codigo: data[i]['codigo'],
              nome: data[i]['nome'],
              valor: data[i]['valor']
            });
          }
        });
    });
  }

  ngOnInit() {}
}
