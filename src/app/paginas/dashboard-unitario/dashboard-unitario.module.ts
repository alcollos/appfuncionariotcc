import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardUnitarioPage } from './dashboard-unitario.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardUnitarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardUnitarioPage]
})
export class DashboardUnitarioPageModule {}
