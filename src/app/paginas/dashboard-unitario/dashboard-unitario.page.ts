import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { NavController, NavParams } from "@ionic/angular";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ServiceUserService } from "../../provider/service-user.service";
import { MenuService } from "../../provider/menu.service";
import { jsonpFactory } from "@angular/http/src/http_module";
import { modelGroupProvider } from "@angular/forms/src/directives/ng_model_group";

@Component({
  selector: "app-dashboard-unitario",
  templateUrl: "./dashboard-unitario.page.html",
  styleUrls: ["./dashboard-unitario.page.scss"]
})
export class DashboardUnitarioPage implements OnInit {
  @ViewChild("lineCanvas") lineCanvas;
  nome: any;
  loading: any;
  detalhe: NavigationExtras;

  produtoItem: Array<{
    codigo: any;
    modelo: any;
    descricao: any;
    foto: any;
    status: any;
    valor: any;
    peso: any;
    palmilha: any;
    marca: any;
    categoria: any;
    codTamanho: any;
    codCor: any;
    codQuantidade: any;
    codCategoria: any;
    codFornecedor: any;
    codMarca: any;
    codPalmilha: any;
    codPeso: any;
    codPromo: any;
    top: any;
    tamanho: any;
    quantidade: any;
    cor: any;
    fornecedor: any;
    promocao: any;
  }>;

  ProdutoItemTodos: Array<{
    codigo: any;
    modelo: any;
    descricao: any;
    foto: any;
    status: any;
    valor: any;
    peso: any;
    palmilha: any;
    marca: any;
    categoria: any;
    codTamanho: any;
    codCor: any;
    codQuantidade: any;
    codCategoria: any;
    codFornecedor: any;
    codMarca: any;
    codPalmilha: any;
    codPeso: any;
    codPromo: any;
    top: any;
    tamanho: any;
    quantidade: any;
    cor: any;
    fornecedor: any;
    promocao: any;
  }>;
  top: number;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService
  ) {
    this.listProdutos();
  }

  ngOnInit() {}

  listProdutos() {
    this.produtoItem = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrlEstoque() + "vendasDashboard.php")
      .pipe(map(res => res.json()))
      .subscribe(
        listDados => {
          for (let i = 0; i < listDados.length; i++) {
            this.produtoItem.push({
              codigo: listDados[i]["codigo"],
              modelo: listDados[i]["modelo"],
              quantidade: listDados[i]["quantidade"], //
              descricao: listDados[i]["descricao"],
              status: listDados[i]["status"],
              valor: listDados[i]["valor"],
              cor: listDados[i]["cor"], //
              palmilha: listDados[i]["palmilha"],
              marca: listDados[i]["marca"],
              categoria: listDados[i]["categoria"],
              tamanho: listDados[i]["tamanho"], //
              fornecedor: listDados[i]["fornecedor"],
              promocao: listDados[i]["promocao"],
              peso: listDados[i]["peso"],
              foto: listDados[i]["foto"],
              codCor: listDados[i]["codCor"],
              codQuantidade: listDados[i]["codQuantidade"],
              codTamanho: listDados[i]["codTamanho"],
              codCategoria: listDados[i]["codCategoria"],
              codFornecedor: listDados[i]["codFornecedor"],
              codMarca: listDados[i]["codMarca"],
              codPalmilha: listDados[i]["codPalmilha"],
              codPeso: listDados[i]["codPeso"],
              codPromo: listDados[i]["codPromo"],
              top: i + 1
            });
          }
          console.log(this.produtoItem[0].codigo);
          this.ProdutoItemTodos = this.produtoItem;
        },
        error => {
          this.serviceUrl.alertas(
            "Atenção",
            "não possível carregar os produtos, verifique sua conexão!"
          );
          this.serviceUrl.fecharLoding();
        }
      );
    this.serviceUrl.fecharLoding();
  }

  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items

    if (val && val.trim() != "") {
      this.produtoItem = this.ProdutoItemTodos.filter(produto => {
        return produto.modelo.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.produtoItem = this.ProdutoItemTodos;
    }
    console.log(this.produtoItem);
  }

  doRefresh(event) {
    console.log("Begin async operation");
    this.listProdutos();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  mandaId(produto) {
    this.nav.navigateForward("unitario/" + produto.codigo);
  }
}
