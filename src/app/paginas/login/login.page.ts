import { Component, OnInit } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { Http } from "@angular/http";
import { ServiceUserService } from "../../provider/service-user.service";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  authForm: FormGroup;
  autenticando: FormGroup;

  emailUser: any;
  senhaUser: any;
  nomeUser: any;

  configs = {
    isSignIn: true,
    action: "Entre",
    actionChange: "Contate seu supervisor"
  };

  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUser: ServiceUserService,
    private fb: FormBuilder
  ) {
    if (localStorage.getItem("deslogado") == "sim") {
      localStorage.setItem("deslogado", "não");
      location.reload();
    }

    if (localStorage.getItem("user_logado") != null) {
      console.log("autenticação");
      this.autenticar();
      this.nav.navigateForward("home");
    }
  }
  ngOnInit(): void {
    // Declara que um form está sendo criado
    this.createForm();
  }
  private createForm(): void {
    this.autenticando = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      // Validador que atribui uma validação obrigatória, e valida um email, vendo se ele atinge as obrigações necessárias ..@.com

      senha: ["", [Validators.required, Validators.minLength(2)]]
      // Validador que atribui uma validação obrigatória, e um espaço de caracteres de 6 digitos
    });
  }

  async logar() {
    console.log("vamos autenticar");
    if (this.email == undefined || this.senha == undefined) {
      this.urlService.alertas("Atenção", "Preencha todos os campos!");
    } else {
      this.urlService.exibirLoading();

      this.http
        .get(
          this.urlService.getUrlLogin() +
            "loginFuncionario.php?email=" +
            this.email.value +
            "&senha=" +
            this.senha.value
        )
        .pipe(map(res => res.json()))
        .subscribe(
          data => {
            if (data.msg.logado == "sim") {
              if (data.dados.status == "1") {
                this.serviceUser.setFuncId(data.dados.cod_func);
                this.serviceUser.setFuncNome(data.dados.nome);
                this.serviceUser.setFuncStatus(data.dados.status);
                this.serviceUser.setFuncNivel(data.dados.nivel);
                this.serviceUser.setFuncFoto(data.dados.foto);
                this.serviceUser.setFuncEmail(data.dados.email);

                localStorage.setItem("idFunc", data.dados.cod_func);
                localStorage.setItem("NameFunc", data.dados.nome);
                localStorage.setItem("nivelFunc", data.dados.status);
                localStorage.setItem("setFuncFoto", data.dados.foto);
                localStorage.setItem("emailFunc", data.dados.email);
                localStorage.setItem("nivelFunc", data.dados.nivel);

                this.nav.navigateBack("home");
                this.urlService.fecharLoding();
                localStorage.setItem("user_logado", data);
              } else {
                this.urlService.fecharLoding();
                this.urlService.alertas("Atenção", "Usuário Bloqueado!");
              }
            } else {
              this.urlService.fecharLoding();
              this.urlService.alertas(
                "Atenção",
                "Usuário ou senha incorretos!"
              );
            }
          },
          error => {
            this.urlService.fecharLoding();
            this.urlService.alertas(
              "Atenção",
              "Algo deu erro, verifique sua conexão com a internet"
            );
          }
        );
    }
  }

  get nome(): FormControl {
    this.nomeUser = this.autenticando.get("nome");
    return this.autenticando.get("nome") as FormControl;
  }

  get email(): FormControl {
    this.emailUser = this.autenticando.get("email");
    return this.autenticando.get("email") as FormControl;
  }

  get senha(): FormControl {
    this.senhaUser = this.autenticando.get("senha");
    return this.autenticando.get("senha") as FormControl;
  }

  changeAuthAction(): void {
    this.configs.isSignIn = !this.configs.isSignIn;

    // Cria uma constante, que irá estipular o valor já alterado, esse valor será usando nas devidas validações de alternancia
    const { isSignIn } = this.configs;

    // Define qual ação está sendo feita, caso seja verdadeiro ou falso
    this.configs.action = isSignIn ? "Entre" : "Contate o supervisor";
    this.configs.actionChange = isSignIn
      ? "Não possui uma conta?"
      : "Eu já tenho uma conta";

    // Adicionando o namecontrol, se não estiver na operação de entrar
  }

  autenticar() {
    this.serviceUser.setFuncId(localStorage.getItem("idFunc"));
    this.serviceUser.setFuncNome(localStorage.getItem("NameFunc"));
    this.serviceUser.setFuncStatus(localStorage.getItem("statusFunc"));
    this.serviceUser.setFuncNivel(localStorage.getItem("nivelFunc"));
    this.serviceUser.setFuncFoto(localStorage.getItem("setFuncFoto"));
  }
}
