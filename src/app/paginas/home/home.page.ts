import { Component } from "@angular/core";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { NavController, NavParams } from "@ionic/angular";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ServiceUserService } from "../../provider/service-user.service";
import { MenuService } from "../../provider/menu.service";
import { jsonpFactory } from "@angular/http/src/http_module";
import { modelGroupProvider } from "@angular/forms/src/directives/ng_model_group";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  nome: any;
  loading: any;
  detalhe: NavigationExtras;

  produtoItem: Array<{
    codigo: any;
    modelo: any;
    descricao: any;
    foto: any;
    status: any;
    valor: any;
    peso: any;
    palmilha: any;
    marca: any;
    categoria: any;
    codTamanho: any;
    codCor: any;
    codQuantidade: any;
    codCategoria: any;
    codFornecedor: any;
    codMarca: any;
    codPalmilha: any;
    codPeso: any;
    codPromo: any;

    tamanho: any;
    quantidade: any;
    cor: any;
    fornecedor: any;
    promocao: any;
  }>;
  produtoItemTodos: Array<{
    codigo: any;
    modelo: any;
    descricao: any;
    foto: any;
    status: any;
    valor: any;
    peso: any;
    palmilha: any;
    marca: any;
    categoria: any;
    codTamanho: any;
    codCor: any;
    codQuantidade: any;
    codCategoria: any;
    codFornecedor: any;
    codMarca: any;
    codPalmilha: any;
    codPeso: any;
    codPromo: any;

    tamanho: any;
    quantidade: any;
    cor: any;
    fornecedor: any;
    promocao: any;
  }>;

  constructor(
    // tslint:disable-next-line: deprecation
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService
  ) {
    this.validarMenu();
    this.listProdutos();
    console.log(this.serviceUser.getFuncId());
  }

  // Verifica qual o nivel do usuário, para poder ter acesso a determinadfas funções
  validarMenu() {
    if (localStorage.getItem("user_logado") != null) {
      console.log("usuario logado");
      this.serviceUser.setFuncId(localStorage.getItem("idFunc"));
      this.serviceUser.setFuncNome(localStorage.getItem("NameFunc"));
      this.serviceUser.setFuncStatus(localStorage.getItem("statusFunc"));
      this.serviceUser.setFuncNivel(localStorage.getItem("nivelFunc"));
      this.serviceUser.setFuncFoto(localStorage.getItem("setFuncFoto"));
    } else {
      this.nav.navigateBack("login");
    }
    if (this.serviceUser.funcNivel === "2") {
      this.menuService.perfilMenu.push(this.menuService.menu[2]); // selecionados
      this.menuService.perfilMenu.push(this.menuService.menu[3]); // caixa
      this.menuService.perfilMenu.push(this.menuService.menu[0]); // publicar produto
      this.menuService.perfilMenu.push(this.menuService.menu[4]); // estoque
    }
    if (this.serviceUser.funcNivel === "1") {
      this.menuService.perfilMenu.push(this.menuService.menu[0]); // publicar produto
      this.menuService.perfilMenu.push(this.menuService.menu[1]); // cadastrar usuario
      this.menuService.perfilMenu.push(this.menuService.menu[2]); // selecionados
      this.menuService.perfilMenu.push(this.menuService.menu[3]); // caixa
      this.menuService.perfilMenu.push(this.menuService.menu[4]); // estoque
      this.menuService.perfilMenu.push(this.menuService.menu[5]); // dashboardPrincipal
      this.menuService.perfilMenu.push(this.menuService.menu[6]); // dashboardSatisfacao
      this.menuService.perfilMenu.push(this.menuService.menu[7]); // dashboardUnitario
    }
  }

  // Pega os dados do servidor, api (php)
  listProdutos() {
    this.produtoItem = [];
    this.serviceUrl.exibirLoading();
    this.http
      .get(this.serviceUrl.getUrlEstoque() + "listDados.php")
      .pipe(map(res => res.json()))
      .subscribe(
        listDados => {
          for (let i = 0; i < listDados.length; i++) {
            this.produtoItem.push({
              codigo: listDados[i]["codigo"],
              modelo: listDados[i]["modelo"],
              quantidade: listDados[i]["quantidade"], //
              descricao: listDados[i]["descricao"],
              status: listDados[i]["status"],
              valor: listDados[i]["valor"],
              cor: listDados[i]["cor"], //
              palmilha: listDados[i]["palmilha"],
              marca: listDados[i]["marca"],
              categoria: listDados[i]["categoria"],
              tamanho: listDados[i]["tamanho"], //
              fornecedor: listDados[i]["fornecedor"],
              promocao: listDados[i]["promocao"],
              peso: listDados[i]["peso"],
              foto: listDados[i]["foto"],
              codCor: listDados[i]["codCor"],
              codQuantidade: listDados[i]["codQuantidade"],
              codTamanho: listDados[i]["codTamanho"],
              codCategoria: listDados[i]["codCategoria"],
              codFornecedor: listDados[i]["codFornecedor"],
              codMarca: listDados[i]["codMarca"],
              codPalmilha: listDados[i]["codPalmilha"],
              codPeso: listDados[i]["codPeso"],
              codPromo: listDados[i]["codPromo"]
            });
          }
          console.log(this.produtoItem[0].codigo);
          this.produtoItemTodos = this.produtoItem;
        },
        error => {
          this.serviceUrl.alertas(
            "Atenção",
            "não possível carregar os produtos, verifique sua conexão!"
          );
          this.serviceUrl.fecharLoding();
        }
      );
    this.serviceUrl.fecharLoding();
  }

  postData(valor) {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http.post(
      this.serviceUrl.getUrlCarrinho() + "adicionarCarrinho.php",
      valor,
      {
        headers: headers,
        method: "POST"
      }
    );
  }

  toProdutos() {
    this.nav.navigateForward("/cadastro_produtos");
  }

  go(id) {
    console.log(id);
    this.route.navigateByUrl("/list_produtos/id");
  }

  // Pegar os dados dos itens do servidor para fazer o search conforme os dados disponiveis
  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items

    if (val && val.trim() != "") {
      this.produtoItem = this.produtoItemTodos.filter(produto => {
        return produto.modelo.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.produtoItem = this.produtoItemTodos;
    }
    console.log(this.produtoItem);
  }

  // Metodo utilizado para dar o refresh nos itens no psuh da tela
  doRefresh(event) {
    console.log("Begin async operation");
    this.listProdutos();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  delete(codigo) {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "deleteProduto.php", codigo, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  deleteProduto(produto) {
    this.delete(produto.codigo).subscribe(
      data => {
        console.log("produto deletado com sucesso");
        this.listProdutos();
      },
      error => {
        console.log("erro ao tentar excluir" + error);
      }
    );
  }

  editProduto(produto) {
    this.detalhe = {
      queryParams: {
        codProd: produto.codigo,
        nome: produto.nome,
        valor: produto.valor,
        descricao: produto.descricao,
        codCateg: produto.codCategoria,
        categoria: produto.categoria,
        codCor: produto.codCor,
        cor: produto.cor,
        codFornecedor: produto.codFornecedor,
        fornecedor: produto.fornecedor,
        foto: produto.foto,
        codMarca: produto.codMarca,
        marca: produto.marca,

        modelo: produto.modelo,
        codPalmilha: produto.codPalmilha,
        palmilha: produto.palmilha,
        codPeso: produto.codPeso,
        peso: produto.peso,
        promocao: produto.promocao,
        codQtd: produto.codQtd,
        quantidade: produto.quantidade,
        status: produto.quantidade,
        codTamanho: produto.codTamanho,
        tamanho: produto.tamanho
      }
    };

    this.route.navigate(["edit-produtos"], this.detalhe);
  }
}
