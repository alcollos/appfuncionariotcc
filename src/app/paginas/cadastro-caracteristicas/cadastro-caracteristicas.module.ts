import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CadastroCaracteristicasPage } from './cadastro-caracteristicas.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroCaracteristicasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CadastroCaracteristicasPage]
})
export class CadastroCaracteristicasPageModule {}
