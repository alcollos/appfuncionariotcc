import { Component, ViewChild, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { UrlService } from "../../provider/url.service";
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators,
  FormGroup
} from "@angular/forms";

import { NavController } from "@ionic/angular";
import { GetServiceService } from "src/app/provider/get/get-service.service";
import { GraficoDiarioService } from "src/app/provider/grafico-diario.service";
import { updateBinding } from "@angular/core/src/render3/instructions";
import { GraficoMensalService } from "src/app/provider/grafico-mensal/grafico-mensal.service";
import { GraficoAnualService } from "src/app/provider/grafico-anual/grafico-anual.service";

@Component({
  selector: "app-teste-dashboard",
  templateUrl: "./teste-dashboard.page.html",
  styleUrls: ["./teste-dashboard.page.scss"]
})
export class TesteDashboardPage implements OnInit {
  @ViewChild("lineCanvas") lineCanvas;
  @ViewChild("lineCanvas2") lineCanvas2;
  @ViewChild("lineCanvas3") lineCanvas3;

  qtdTotal: any;
  qtdMensal: Array<{
    quantidade: any;
    valor: any;
    mes: any;
  }>;
  qtdDiaria: Array<{
    quantidade: any;
    dia: any;
    mes: any;
  }>;
  qtdAnual: Array<{
    quantidade: any;
    valor: any;
    ano: any;
  }>;

  qtdDia: any;

  dataSelecionada: any;
  mesSelecionado: any;
  anoSelecionado: any;
  diaSelecionado: any;

  dia: FormGroup;

  lineChart: any;
  lineChart2: any;
  lineChart3: any;

  qtd1: any;
  qtd2: any;
  qtd3: any;
  qtd4: any;
  qtd5: any;
  qtd6: any;
  qtd7: any;
  qtd8: any;
  qtd9: any;
  qtd10: any;
  qtd11: any;
  qtd12: any;
  qtd13: any;
  qtd14: any;
  qtd15: any;
  qtd16: any;
  qtd17: any;
  qtd18: any;
  qtd19: any;
  qtd20: any;
  qtd21: any;
  qtd22: any;
  qtd23: any;
  qtd24: any;
  qtd25: any;
  qtd26: any;
  qtd27: any;
  qtd28: any;
  qtd29: any;
  qtd30: any;
  qtd31: any;

  mes: any;
  qtdJaneiro: any;
  qtdFevereiro: any;
  qtdMarco: any;
  qtdAbril: any;
  qtdMaio: any;
  qtdJunho: any;
  qtdSetembro: any;
  qtdAgosto: any;
  qtdOutubro: any;
  qtdNovembro: any;
  qtdDezembro: any;
  qtdJulho: any;

  valorJaneiro: any;
  valorFevereiro: any;
  valorMarco: any;
  valorAbril: any;
  valorMaio: any;
  valorJunho: any;
  valorSetembro: any;
  valorAgosto: any;
  valorOutubro: any;
  valorNovembro: any;
  valorDezembro: any;
  valorJulho: any;

  qtd2010: any;
  qtd2011: any;
  qtd2012: any;
  qtd2013: any;
  qtd2014: any;
  qtd2015: any;
  qtd2016: any;
  qtd2017: any;
  qtd2018: any;
  qtd2019: any;

  valor2010: any;
  valor2011: any;
  valor2012: any;
  valor2013: any;
  valor2014: any;
  valor2015: any;
  valor2016: any;
  valor2017: any;
  valor2018: any;
  valor2019: any;
  data: any;
  aMediaMensalVendas: Array<{
    mediaValor: any;
  }>;
  aMediaMensalQTD: Array<{
    mediaQtd: any;
  }>;

  mediaMensalVendas: any;
  mediaMensalQTD: any;
  hora: any;
  minutos: any;
  segundos: any;
  day: any;
  ano: any;
  constructor(
    private http: Http,
    public serviceUrl: UrlService,
    public formBuilder: FormBuilder,
    private ativeRouter: ActivatedRoute,
    private route: Router,
    public graficoDiario: GraficoDiarioService,
    public graficoMensal: GraficoMensalService,
    public graficoAnual: GraficoAnualService
  ) {
    this.apresentarQuantidade();
    this.dia = this.formBuilder.group({
      data_final: ["", Validators.required],
      data_inicial: ["", Validators.required],
      dataSelecionada: ["", Validators.required],
      mesSelecionado: ["", Validators.required],
      anoSelecionado: ["", Validators.required]
    });
    this.mediaQtd();
    this.mediaValor();

    this.atualizarChat();
  }

  atualizarChat() {
    setInterval(() => {
      this.selecionarData();
    }, 1000);
    console.log(this.data);
  }

  selecionarData() {
    let data = new Date();
    this.hora = data.getHours();
    this.minutos = data.getMinutes();
    this.segundos = data.getSeconds();
    this.mes = data.getMonth() + 1;
    this.ano = data.getFullYear();
    this.day = data.getDay();
    this.data =
      this.day +
      "/" +
      this.mes +
      "/" +
      this.ano +
      "-" +
      this.hora +
      ":" +
      this.minutos +
      ":" +
      this.segundos;
  }

  lineChartMethodDia() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: [
          "01/" + this.mes,
          "02/" + this.mes,
          "03/" + this.mes,
          "04/" + this.mes,
          "05/" + this.mes,
          "06/" + this.mes,
          "07/" + this.mes,
          "08/" + this.mes,
          "09/" + this.mes,
          "10/" + this.mes,
          "11/" + this.mes,
          "12/" + this.mes,
          "13/" + this.mes,
          "14/" + this.mes,
          "15/" + this.mes,
          "16/" + this.mes,
          "17/" + this.mes,
          "18/" + this.mes,
          "19/" + this.mes,
          "20/" + this.mes,
          "21/" + this.mes,
          "22/" + this.mes,
          "23/" + this.mes,
          "24/" + this.mes,
          "25/" + this.mes,
          "26/" + this.mes,
          "27/" + this.mes,
          "28/" + this.mes,
          "29/" + this.mes,
          "30/" + this.mes,
          "31/" + this.mes
        ],
        datasets: [
          {
            label: "Vendas por Dia",

            fill: false,
            lineTension: 0.1,
            backgroundColor: "#10dc60",
            borderColor: "#10dc60",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#10dc60",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,

            data: [
              this.qtd1,
              this.qtd2,
              this.qtd3,
              this.qtd4,
              this.qtd5,
              this.qtd6,
              this.qtd7,
              this.qtd8,
              this.qtd9,
              this.qtd10,
              this.qtd11
            ]
          }
        ]
      }
    });
  }

  lineChartMethodMes() {
    this.lineChart2 = new Chart(this.lineCanvas2.nativeElement, {
      type: "line",
      data: {
        labels: [
          "Janeiro",
          "Fevereiro",
          "Março",
          "Abril",
          "Maio",
          "Junho",
          "Julho",
          "Agosto",
          "Setembro",
          "Outubro",
          "Novembro",
          "Dezembro"
        ],
        datasets: [
          {
            label: "Quantidade de vendas por mês",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#10dc60",
            borderColor: "#10dc60",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#10dc60",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              this.qtdJaneiro,
              this.qtdFevereiro,
              this.qtdMarco,
              this.qtdAbril,
              this.qtdMaio,
              this.qtdJulho,
              this.qtdJunho,
              this.qtdAgosto,
              this.qtdSetembro,
              this.qtdOutubro,
              this.qtdNovembro,
              this.qtdDezembro
            ],
            spanGaps: false
          },
          {
            label: "Valor vendido por mês R$",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#cae398",
            borderColor: "#cae398",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#cae398",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              this.valorJaneiro,
              +this.valorJaneiro,
              "R$" + this.valorFevereiro,
              this.valorMarco,
              this.valorAbril,
              this.valorMaio,
              this.valorJunho,
              this.valorJulho,
              this.valorAgosto,
              this.valorSetembro,
              this.valorOutubro,
              this.valorNovembro,
              this.valorDezembro
            ],

            spanGaps: false
          }
        ]
      }
    });
  }

  lineChartMethodAno() {
    this.lineChart3 = new Chart(this.lineCanvas3.nativeElement, {
      type: "line",
      data: {
        labels: [
          "2010",
          "2011",
          "2012",
          "2013",
          "2014",
          "2015",
          "2016",
          "2017",
          "2018",
          "2019"
        ],
        datasets: [
          {
            label: "Vendas feitas por ano",

            fill: false,
            lineTension: 0.1,
            backgroundColor: "#10dc60",
            borderColor: "#10dc60",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#10dc60",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              //COLOCAR O VALOR
              this.qtd2010,
              this.qtd2011,
              this.qtd2012,
              this.qtd2013,

              this.qtd2014,
              this.qtd2015,
              this.qtd2016,
              this.qtd2017,
              this.qtd2018,
              this.qtd2019
            ],
            spanGaps: false
          },
          {
            label: "Valor vendido por ano R$",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "#cae398",
            borderColor: "#cae398",
            borderCapStyle: "butt",

            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "#cae398",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 5,
            data: [
              //COLOCAR O VALOR
              this.valor2010,
              this.valor2011,
              this.valor2012,
              this.valor2013,
              this.valor2014,
              this.valor2015,
              this.valor2016,
              this.valor2017,
              this.valor2018,
              this.valor2019
            ],
            spanGaps: false
          }
        ]
      }
    });
  }

  ngOnInit() {
    this.lineChartMethodDia();
    this.lineChartMethodMes();
    this.lineChartMethodAno();
  }

  scriptQuantidadeDiaria() {
    this.qtdDiaria = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getQuantidadeDiaria.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.qtdDiaria.push({
            quantidade: dados[i].quantidade,
            dia: dados[i].dia,
            mes: dados[i].mes
          });
          if (this.qtdDiaria[i].dia === "1") {
            this.qtd1 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "2") {
            this.qtd2 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "3") {
            this.qtd3 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "4") {
            this.qtd4 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "5") {
            this.qtd5 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "6") {
            this.qtd6 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "i") {
            this.qtd7 = this.qtdDiaria[6].quantidade;
          }
          if (this.qtdDiaria[i].dia === "8") {
            this.qtd8 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "9") {
            this.qtd9 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "10") {
            this.qtd10 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "11") {
            this.qtd11 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "12") {
            this.qtd12 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "13") {
            this.qtd13 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "14") {
            this.qtd14 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "15") {
            this.qtd15 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "16") {
            this.qtd16 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "17") {
            this.qtd17 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "18") {
            this.qtd18 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "19") {
            this.qtd19 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "20") {
            this.qtd20 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "21") {
            this.qtd21 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "22") {
            this.qtd22 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "23") {
            this.qtd23 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "24") {
            this.qtd24 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "25") {
            this.qtd25 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "26") {
            this.qtd26 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "27") {
            this.qtd27 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "28") {
            this.qtd28 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "29") {
            this.qtd29 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "30") {
            this.qtd30 = this.qtdDiaria[i].quantidade;
          }
          if (this.qtdDiaria[i].dia === "31") {
            this.qtd31 = this.qtdDiaria[i].quantidade;
          }
          this.mes = this.qtdDiaria[i].mes;
        }
      });
  }

  apresentarQuantidadeDiaria() {
    this.scriptQuantidadeDiaria();
    this.lineChart.data.datasets[0].data[0] = this.qtd1;
    this.lineChart.data.labels[0] = "01/" + this.mes; //FAZER DE TODOS

    this.lineChart.data.datasets[0].data[1] = this.qtd2;

    this.lineChart.data.datasets[0].data[2] = this.qtd3;

    this.lineChart.data.datasets[0].data[3] = this.qtd4;

    this.lineChart.data.datasets[0].data[4] = this.qtd5;

    this.lineChart.data.datasets[0].data[5] = this.qtd6;

    this.lineChart.data.datasets[0].data[6] = this.qtd7;

    this.lineChart.data.datasets[0].data[7] = this.qtd8;

    this.lineChart.data.datasets[0].data[8] = this.qtd9;

    this.lineChart.data.datasets[0].data[9] = this.qtd10;
    this.lineChart.data.datasets[0].data[10] = this.qtd11;
    this.lineChart.data.datasets[0].data[11] = this.qtd12;
    this.lineChart.data.datasets[0].data[12] = this.qtd13;
    this.lineChart.data.datasets[0].data[13] = this.qtd14;
    this.lineChart.data.datasets[0].data[14] = this.qtd15;
    this.lineChart.data.datasets[0].data[15] = this.qtd16;
    this.lineChart.data.datasets[0].data[16] = this.qtd17;
    this.lineChart.data.datasets[0].data[17] = this.qtd18;
    this.lineChart.data.datasets[0].data[18] = this.qtd19;
    this.lineChart.data.datasets[0].data[19] = this.qtd20;
    this.lineChart.data.datasets[0].data[20] = this.qtd21;
    this.lineChart.data.datasets[0].data[21] = this.qtd22;
    this.lineChart.data.datasets[0].data[22] = this.qtd23;
    this.lineChart.data.datasets[0].data[23] = this.qtd24;
    this.lineChart.data.datasets[0].data[24] = this.qtd25;
    this.lineChart.data.datasets[0].data[25] = this.qtd26;
    this.lineChart.data.datasets[0].data[26] = this.qtd27;
    this.lineChart.data.datasets[0].data[27] = this.qtd28;
    this.lineChart.data.datasets[0].data[28] = this.qtd29;
    this.lineChart.data.datasets[0].data[29] = this.qtd30;
    this.lineChart.data.datasets[0].data[30] = this.qtd31;

    this.lineChart.data.labels[0] = "01/" + this.mes;
    this.lineChart.data.labels[1] = "02/" + this.mes;
    this.lineChart.data.labels[2] = "03/" + this.mes;
    this.lineChart.data.labels[3] = "04/" + this.mes;
    this.lineChart.data.labels[4] = "05/" + this.mes;
    this.lineChart.data.labels[5] = "06/" + this.mes;
    this.lineChart.data.labels[6] = "07/" + this.mes;
    this.lineChart.data.labels[7] = "08/" + this.mes;
    this.lineChart.data.labels[8] = "09/" + this.mes;
    this.lineChart.data.labels[9] = "10/" + this.mes;
    this.lineChart.data.labels[10] = "11/" + this.mes;
    this.lineChart.data.labels[11] = "12/" + this.mes;
    this.lineChart.data.labels[12] = "13/" + this.mes;
    this.lineChart.data.labels[13] = "14/" + this.mes;
    this.lineChart.data.labels[14] = "15/" + this.mes;
    this.lineChart.data.labels[15] = "16/" + this.mes;
    this.lineChart.data.labels[16] = "17/" + this.mes;
    this.lineChart.data.labels[17] = "18/" + this.mes;
    this.lineChart.data.labels[18] = "19/" + this.mes;
    this.lineChart.data.labels[19] = "20/" + this.mes;
    this.lineChart.data.labels[20] = "21/" + this.mes;
    this.lineChart.data.labels[21] = "22/" + this.mes;
    this.lineChart.data.labels[22] = "23/" + this.mes;
    this.lineChart.data.labels[23] = "24/" + this.mes;
    this.lineChart.data.labels[24] = "25/" + this.mes;
    this.lineChart.data.labels[25] = "26/" + this.mes;
    this.lineChart.data.labels[26] = "27/" + this.mes;
    this.lineChart.data.labels[27] = "28/" + this.mes;
    this.lineChart.data.labels[28] = "29/" + this.mes;
    this.lineChart.data.labels[29] = "30/" + this.mes;
    this.lineChart.data.labels[30] = "31/" + this.mes;

    this.lineChart.update();
  }

  scriptQuantidadeMes() {
    this.qtdMensal = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getQuantidadeMensal.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.qtdMensal.push({
            quantidade: dados[i].quantidade,
            valor: dados[i].valor,
            mes: dados[i].mes
          });
          if (this.qtdMensal[i].mes === "1") {
            this.qtdJaneiro = this.qtdMensal[i].quantidade;
            this.valorJaneiro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "2") {
            this.qtdFevereiro = this.qtdMensal[i].quantidade;
            this.valorFevereiro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "3") {
            this.qtdMarco = this.qtdMensal[i].quantidade;
            this.valorMarco = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "4") {
            this.qtdAbril = this.qtdMensal[i].quantidade;

            this.valorAbril = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "5") {
            this.qtdMaio = this.qtdMensal[i].quantidade;
            this.valorMaio = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "6") {
            this.qtdJunho = this.qtdMensal[i].quantidade;
            this.valorJunho = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "7") {
            this.qtdJulho = this.qtdMensal[i].quantidade;
            this.valorJulho = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "8") {
            this.qtdAgosto = this.qtdMensal[i].quantidade;
            this.valorAgosto = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "9") {
            this.qtdSetembro = this.qtdMensal[i].quantidade;

            this.valorSetembro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "10") {
            this.qtdOutubro = this.qtdMensal[i].quantidade;
            this.valorOutubro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "11") {
            this.qtdNovembro = this.qtdMensal[i].quantidade;
            this.valorNovembro = this.qtdMensal[i].valor;
          }
          if (this.qtdMensal[i].mes === "12") {
            this.qtdDezembro = this.qtdMensal[i].quantidade;

            this.valorDezembro = this.qtdMensal[i].valor;
          }
        }
      });
  }

  apresentarQuantidadeMes() {
    this.scriptQuantidadeMes();
    this.lineChart2.data.datasets[0].data[0] = this.qtdJaneiro;
    this.lineChart2.data.datasets[1].data[0] = this.valorJaneiro;

    this.lineChart2.data.datasets[0].data[1] = this.qtdFevereiro;
    this.lineChart2.data.datasets[1].data[1] = this.valorFevereiro;

    this.lineChart2.data.datasets[0].data[2] = this.qtdMarco;

    this.lineChart2.data.datasets[0].data[3] = this.qtdAbril;

    this.lineChart2.data.datasets[0].data[4] = this.qtdMaio;

    this.lineChart2.data.datasets[0].data[5] = this.qtdJunho;

    this.lineChart2.data.datasets[0].data[6] = this.qtdJulho;

    this.lineChart2.data.datasets[0].data[7] = this.qtdAgosto;

    this.lineChart2.data.datasets[0].data[8] = this.qtdSetembro;

    this.lineChart2.data.datasets[0].data[9] = this.qtdOutubro;

    this.lineChart2.data.datasets[0].data[10] = this.qtdNovembro;

    this.lineChart2.data.datasets[0].data[11] = this.qtdDezembro;

    this.lineChart2.data.datasets[1].data[2] = this.valorMarco;
    this.lineChart2.data.datasets[1].data[3] = this.valorAbril;
    this.lineChart2.data.datasets[1].data[4] = this.valorMaio;
    this.lineChart2.data.datasets[1].data[5] = this.valorJunho;
    this.lineChart2.data.datasets[1].data[6] = this.valorJulho;
    this.lineChart2.data.datasets[1].data[7] = this.valorAgosto;
    this.lineChart2.data.datasets[1].data[8] = this.valorSetembro;
    this.lineChart2.data.datasets[1].data[9] = this.valorOutubro;
    this.lineChart2.data.datasets[1].data[10] = this.valorNovembro;
    this.lineChart2.data.datasets[1].data[11] = this.valorDezembro;

    this.lineChart2.update();
  }

  scriptQuantidadeAno() {
    this.qtdAnual = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "getQuantidadeAnual.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.qtdAnual.push({
            quantidade: dados[i].quantidade,
            valor: dados[i].valor,
            ano: dados[i].ano
          });
          if (this.qtdAnual[i].ano === "2010") {
            this.qtd2010 = this.qtdAnual[i].quantidade;
            this.valor2010 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2011") {
            this.qtd2011 = this.qtdAnual[i].quantidade;
            this.valor2011 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2012") {
            this.qtd2012 = this.qtdAnual[i].quantidade;
            this.valor2012 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2013") {
            this.qtd2013 = this.qtdAnual[i].quantidade;
            this.valor2013 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2014") {
            this.qtd2014 = this.qtdAnual[i].quantidade;
            this.valor2014 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2015") {
            this.qtd2015 = this.qtdAnual[i].quantidade;
            this.valor2015 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2016") {
            this.qtd2016 = this.qtdAnual[i].quantidade;
            this.valor2016 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2017") {
            this.qtd2017 = this.qtdAnual[i].quantidade;
            this.valor2017 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2018") {
            this.qtd2018 = this.qtdAnual[i].quantidade;
            this.valor2018 = this.qtdAnual[i].valor;
          }
          if (this.qtdAnual[i].ano === "2019") {
            this.qtd2019 = this.qtdAnual[i].quantidade;
            this.valor2019 = this.qtdAnual[i].valor;
          }
        }
      });
  }

  apresentarQuantidadeAno() {
    this.scriptQuantidadeAno();
    this.lineChart3.data.datasets[0].data[0] = this.qtd2010;
    this.lineChart3.data.datasets[1].data[0] = this.valor2010;

    this.lineChart3.data.datasets[0].data[1] = this.qtd2011;
    this.lineChart3.data.datasets[1].data[1] = this.valor2011;

    this.lineChart3.data.datasets[0].data[2] = this.qtd2012;

    this.lineChart3.data.datasets[0].data[3] = this.qtd2013;

    this.lineChart3.data.datasets[0].data[4] = this.qtd2014;

    this.lineChart3.data.datasets[0].data[5] = this.qtd2015;

    this.lineChart3.data.datasets[0].data[6] = this.qtd2016;

    this.lineChart3.data.datasets[0].data[7] = this.qtd2017;

    this.lineChart3.data.datasets[0].data[8] = this.qtd2018;

    this.lineChart3.data.datasets[0].data[9] = this.qtd2019;

    this.lineChart3.data.datasets[1].data[2] = this.valor2012;
    this.lineChart3.data.datasets[1].data[3] = this.valor2013;
    this.lineChart3.data.datasets[1].data[4] = this.valor2014;
    this.lineChart3.data.datasets[1].data[5] = this.valor2015;
    this.lineChart3.data.datasets[1].data[6] = this.valor2016;
    this.lineChart3.data.datasets[1].data[7] = this.valor2017;
    this.lineChart3.data.datasets[1].data[8] = this.valor2018;
    this.lineChart3.data.datasets[1].data[9] = this.valor2019;

    this.lineChart3.update();
  }

  mediaQtd() {
    this.aMediaMensalQTD = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "mediaMensalQTD.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aMediaMensalQTD.push({
            mediaQtd: dados[i].mediaQtd
          });
        }
        this.mediaMensalQTD = this.aMediaMensalQTD[0].mediaQtd;
      });
  }

  mediaValor() {
    this.aMediaMensalVendas = [];
    this.http
      .get(
        this.serviceUrl.getUrlSelect() +
          "mediaMensalVendas.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aMediaMensalVendas.push({
            mediaValor: dados[i].mediaValor
          });
        }
        this.mediaMensalVendas = this.aMediaMensalVendas[0].mediaValor;
      });
  }

  // TUDO ABAIXO FOI/É UTILIZADO EM TESTES

  apresentarQuantidade() {
    this.serviceUrl.exibirLoading();

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASH.php" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdTotal = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  apresentarQTDDIA() {
    this.atualizarDashboard();
    this.apresentarAll();
    this.serviceUrl.exibirLoading();

    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "getQuantidadeDASHDiaria.php?data_selecionada=" +
          this.dataSelecionada
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.qtdDia = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  apresentarAll() {
    setInterval(() => {
      this.apresentarQuantidadeAno();
      this.apresentarQuantidadeDiaria();
      this.apresentarQuantidadeMes();
    }, 1000);
    console.log(this.data);
  }

  atualizarDashboard() {
    this.qtdJaneiro = 0;
    this.qtdFevereiro = 0;
    this.qtdMarco = 0;
    this.qtdAbril = 0;
    this.qtdMaio = 0;
    this.qtdJulho = 0;
    this.qtdJunho = 0;
    this.qtdAgosto = 0;
    this.qtdSetembro = 0;
    this.qtdOutubro = 0;
    this.qtdNovembro = 0;
    this.qtdDezembro = 0;

    this.valorJaneiro = 0;
    this.valorFevereiro = 0;
    this.valorMarco = 0;
    this.valorAbril = 0;
    this.valorMaio = 0;
    this.valorJunho = 0;
    this.valorJulho = 0;
    this.valorAgosto = 0;
    this.valorSetembro = 0;
    this.valorOutubro = 0;
    this.valorNovembro = 0;
    this.valorDezembro = 0;

    this.qtd1 = 0;
    this.qtd2 = 0;
    this.qtd3 = 0;
    this.qtd4 = 0;
    this.qtd5 = 0;
    this.qtd6 = 0;
    this.qtd7 = 0;
    this.qtd8 = 0;
    this.qtd9 = 0;
    this.qtd10 = 0;
    this.qtd10 = 0;
    this.qtd11 = 0;
    this.qtd12 = 0;
    this.qtd13 = 0;
    this.qtd14 = 0;
    this.qtd15 = 0;
    this.qtd16 = 0;
    this.qtd17 = 0;
    this.qtd18 = 0;
    this.qtd19 = 0;
    this.qtd20 = 0;
    this.qtd21 = 0;
    this.qtd22 = 0;
    this.qtd23 = 0;
    this.qtd24 = 0;
    this.qtd25 = 0;
    this.qtd26 = 0;
    this.qtd27 = 0;
    this.qtd28 = 0;
    this.qtd29 = 0;
    this.qtd30 = 0;
    this.qtd31 = 0;

    this.qtd2010 = 0;
    this.qtd2011 = 0;
    this.qtd2012 = 0;
    this.qtd2013 = 0;
    this.qtd2014 = 0;
    this.qtd2015 = 0;
    this.qtd2016 = 0;
    this.qtd2017 = 0;
    this.qtd2018 = 0;
    this.qtd2019 = 0;

    this.valor2010 = 0;
    this.valor2011 = 0;
    this.valor2012 = 0;
    this.valor2013 = 0;
    this.valor2014 = 0;
    this.valor2015 = 0;
    this.valor2016 = 0;
    this.valor2017 = 0;
    this.valor2018 = 0;
    this.valor2019 = 0;

    this.apresentarQuantidadeAno();
    this.apresentarQuantidadeDiaria();
    this.apresentarQuantidadeMes();
  }
}
