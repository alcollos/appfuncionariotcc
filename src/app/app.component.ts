import { Component } from "@angular/core";

import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { ServiceUserService } from "../app/provider/service-user.service";
import { UrlService } from "../app/provider/url.service";
import { MenuService } from "../app/provider/menu.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "home"
    },
    {
      title: "List",
      url: "/list",
      icon: "list"
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public seriveUser: ServiceUserService,
    public urlService: UrlService,
    public navController: NavController,
    public menuService: MenuService
  ) {
    this.seriveUser.getFuncNome();
    this.seriveUser.getFuncFoto();
    this.urlService.getUrlCadastro();
    this.urlService.getUrlCarrinho();
    this.urlService.getUrlLogin();
    this.urlService.getUrlEstoque();

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // Para redirecionar para uma pagina em especifico
  acaoMenu(menu) {
    switch (menu) {
      case "publicar":
        this.publicar();
        break;

      case "cadastrar-usuarios":
        this.cadastrarUsuario();
        break;

      case "selecionados":
        this.selecionado();
        break;

      case "caixa":
        this.caixa();
        break;
      case "estoque":
        this.estoque();
        break;
      case "dashboardPrincipal":
        this.dashboardPrincipal();
        break;
      case "dashboardSatisfacao":
        this.dashboardSatisfacao();
        break;
      case "dashboardUnitario":
        this.dashboardUnitario();
        break;

      default:
        break;
    }
  }

  // Pagina que serão redirecionadas

  publicar() {
    this.navController.navigateForward("cadastro_produtos");
  }

  cadastrarUsuario() {
    this.navController.navigateForward("cadastrar-usuarios");
  }

  cadastroEmpresa() {
    this.navController.navigateForward("cadastro_produtos");
  }

  selecionado() {
    this.navController.navigateForward("selecionados");
  }

  caixa() {
    this.navController.navigateForward("caixa");
  }
  estoque() {
    this.navController.navigateForward("home");
  }
  dashboardPrincipal() {
    this.navController.navigateForward("teste-dashboard");
  }
  dashboardSatisfacao() {
    this.navController.navigateForward("dashboard-satisfacao");
  }
  dashboardUnitario() {
    this.navController.navigateForward("dashboard-unitario");
  }

  // logout
  sair() {
    localStorage.clear();
    location.reload();
    this.navController.navigateRoot("login");
    localStorage.setItem("deslogado", "sim");
  }
}
