import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { HttpModule } from "@angular/http";

import { IonicModule, IonicRouteStrategy, NavParams } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { Base64 } from "@ionic-native/base64/ngx";
import { ImagePicker } from "@ionic-native/image-picker/ngx";

import { BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Ndef, NFC } from "@ionic-native/nfc/ngx";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,

    BarcodeScanner,
    NFC,
    Ndef,
    ReactiveFormsModule,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Base64,
    ImagePicker
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
