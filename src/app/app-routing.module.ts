import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "home",
    loadChildren: "./paginas/home/home.module#HomePageModule"
  },

  {
    path: "list_produtos/:id",
    loadChildren:
      "./paginas/list-produtos/list-produtos.module#ListProdutosPageModule"
  },
  {
    path: "list_usuarios",
    loadChildren:
      "./paginas/list-usuarios/list-usuarios.module#ListUsuariosPageModule"
  },
  {
    path: "cadastro_produtos",
    loadChildren:
      "./paginas/cadastro-produtos/cadastro-produtos.module#CadastroProdutosPageModule"
  },

  {
    path: "login",
    loadChildren: "./paginas/login/login.module#LoginPageModule"
  },

  {
    path: "cadastrar-usuarios",
    loadChildren:
      "./paginas/cadastrar-usuarios/cadastrar-usuarios.module#CadastrarUsuariosPageModule"
  },

  {
    path: "selecionados",
    loadChildren:
      "./paginas/selecionados/selecionados.module#SelecionadosPageModule"
  },
  {
    path: "list-selecionados/:id",
    loadChildren:
      "./paginas/list-selecionados/list-selecionados.module#ListSelecionadosPageModule"
  },
  {
    path: "caixa",
    loadChildren: "./paginas/caixa/caixa.module#CaixaPageModule"
  },
  {
    path: "list-finalizacao/:id",
    loadChildren:
      "./paginas/list-finalizacao/list-finalizacao.module#ListFinalizacaoPageModule"
  },
  {
    path: "edit-produtos",
    loadChildren:
      "./paginas/edit-produtos/edit-produtos.module#EditProdutosPageModule"
  },
  {
    path: "baixa-quantidade",
    loadChildren:
      "./paginas/baixa-quantidade/baixa-quantidade.module#BaixaQuantidadePageModule"
  },
  {
    path: "dashboard",
    loadChildren: "./paginas/dashboard/dashboard.module#DashboardPageModule"
  },
  {
    path: "teste-dashboard",
    loadChildren:
      "./paginas/teste-dashboard/teste-dashboard.module#TesteDashboardPageModule"
  },
  {
    path: "dashboard-unitario",
    loadChildren:
      "./paginas/dashboard-unitario/dashboard-unitario.module#DashboardUnitarioPageModule"
  },
  {
    path: "unitario/:id",
    loadChildren: "./paginas/unitario/unitario.module#UnitarioPageModule"
  },
  { path: 'dashboard-satisfacao', loadChildren: './paginas/dashboard-satisfacao/dashboard-satisfacao.module#DashboardSatisfacaoPageModule' },
  { path: 'cadastro-caracteristicas', loadChildren: './paginas/cadastro-caracteristicas/cadastro-caracteristicas.module#CadastroCaracteristicasPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
